
state = {
	id = 195
	name = "STATE_195"

	history = {
		owner = YEM
		victory_points = { 10840 10 } #Sana'a
		victory_points = { 9217 5 } #Sa'dah
		
		buildings = {
			infrastructure = 3
			internet_station = 1
			industrial_complex = 1
			air_base = 2
			3469 = {
				naval_base = 2

			}

		}
		add_core_of = YEM
		2017.1.1 = {
			buildings = {
				internet_station = 2
			}
			owner = HOU
			add_core_of = HOU
			add_core_of = AQY
			add_manpower = 2000000
			add_manpower = 1350000
			add_manpower = 321608
			buildings = {
				arms_factory = 1
			}
		}
	}

	provinces = {
		10840 3469 9147 9189 9199
	}
	manpower = 6410221
	buildings_max_level_factor = 1.000
	state_category = state_07
}

state = {
	id = 967
	name = "STATE_967"
	manpower = 1109305
	state_category = state_02

	resources = {
	}
	history = {
		owner = GER
		victory_points = {
			9575 1
		}
		victory_points = {
			11531 1
		}
		buildings = {
			infrastructure = 6
			internet_station = 2
			industrial_complex = 1
		}
		add_core_of = GER
		2017.1.1 = {
			add_manpower = 11366
			buildings = {
				internet_station = 4
				industrial_complex = 1
			}
		}
	}

	provinces = {
		9575 11531
	}
}

state = {
	id = 91
	name = "STATE_91"
	resources = {
	}
	history = {
		owner = SPR
		victory_points = {
			3938 15
		}
		buildings = {
			infrastructure = 5
			internet_station = 2
			arms_factory = 3
			industrial_complex = 1
			air_base = 6

		}
		add_core_of = SPR
		2017.1.1 = {
			add_manpower = 1240010
			buildings = {
				internet_station = 4
				industrial_complex = 2
			}
		}
	}

	provinces = {
		896 3938 6993 9767 9785
	}
	manpower = 5423384	
	buildings_max_level_factor = 1.000
	state_category = state_06
}

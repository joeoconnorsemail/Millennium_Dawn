state = {
	id = 41
	name = "STATE_41"
	manpower = 7117088
	state_category = state_07

	resources = {
		aluminium = 22
		steel = 26
	}
	history = {
		owner = GER
		victory_points = {
			6444 3
		}
		victory_points = {
			6488 10
		}
		victory_points = {
			564 1
		}
		buildings = {
			infrastructure = 6
			internet_station = 2
			arms_factory = 2
			industrial_complex = 5
			synthetic_refinery = 2
		}
		add_core_of = GER
		2017.1.1 = {
			add_manpower = 69258
			buildings = {
				internet_station = 4
				industrial_complex = 2
			}
		}
	}

	provinces = {
		564 3397 3524 3574 6488 6549 9486 9524 11445 11533 529 6444
	}
}
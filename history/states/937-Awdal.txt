
state = {
	id = 937
	name = "STATE_937"

	history = {
		owner = SML
		victory_points = {
			10777 2
		}
		buildings = {
			infrastructure = 1
		}
		add_core_of = SML
		add_core_of = SOM
		add_core_of = SNA
		2017.1.1 = {
			remove_core_of = SNA
			add_manpower = 300000
		}
	}

	provinces = {
		10777
	}
	manpower = 450000
	buildings_max_level_factor = 1.000
	state_category = state_03
}

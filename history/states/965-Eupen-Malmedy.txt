
state = {
	id = 965
	name = "STATE_965"
	
	resources = {
	}

	history = {
		owner = BEL
		buildings = {
			infrastructure = 6
			internet_station = 2
			arms_factory = 1

		}
		add_core_of = BEL
		2017.1.1 = {
			add_manpower = 30000
			buildings = {
				internet_station = 4
			}
		}
	}

	provinces = {
		3488
	}
	manpower = 273086
	buildings_max_level_factor = 1.000
	state_category = state_00
}

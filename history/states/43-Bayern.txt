state = {
	id = 43
	name = "STATE_43"
	manpower = 8738436
	state_category = state_08

	resources = {
		steel = 60
	}
	history = {
		owner = GER
		victory_points = {
			692 10
		}
		victory_points = {
			9666 5
		}
		victory_points = {
			9515 1
		}
		victory_points = {
			9681 1
		}
		buildings = {
			infrastructure = 7
			internet_station = 2
			arms_factory = 5
			industrial_complex = 4
			air_base = 6
		}
		add_core_of = GER
		2017.1.1 = {
			add_manpower = 181517
			buildings = {
				internet_station = 4
				industrial_complex = 5
			}
		}
	}

	provinces = {
		532 571 586 589 692 707 708 3299 3541 3571 3688 3705 6693 9515 9652 9666 9681 11497 11620 11638 11653 342
	}
}
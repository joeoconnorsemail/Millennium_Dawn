﻿capital = 206

oob = "BLR_2000"

set_convoys = 5
set_stability = 0.5

set_country_flag = country_language_belarusian
set_country_flag = country_language_russian

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	AT_upgrade_1 = 1
	Heavy_Anti_tank_1 = 1
	Anti_tank_1 = 1 
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1

	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_decline
	limited_conscription
}

set_politics = {

	parties = {
		
		nationalist = {
			popularity = 10
		}
		reactionary = {
			popularity = 30
		}
		conservative = {
			popularity = 20
		}
		market_liberal = {
			popularity = 5
		}
		social_liberal = {
			popularity = 5
		}
		social_democrat = {
			popularity = 10
		}
		communist = {
			popularity = 20
		}
		
	}
	
	ruling_party = reactionary
	last_election = "1997.1.1"
	election_frequency = 60
	elections_allowed = yes
}

create_country_leader = {
	name = "Alexander Lukashenko"
	picture = "Alexander_Lukashenko.dds"
	ideology = oligarchist
}

create_country_leader = {
	name = "Sergei Gaidukevich"
	picture = "Sergei_Gaidukevich.dds"
	ideology = autocrat
}

create_country_leader = {
	name = "Vincuk Viacorka"
	picture = "Vincuk_Viacorka.dds"
	ideology = constitutionalist
}

create_country_leader = {
	name = "Paval Sieviaryniets"
	picture = "Paval_Sieviaryniets.dds"
	ideology = libertarian
}

create_country_leader = {
	name = "Vladimir Novosiad"
	picture = "Vladimir_Novosiad.dds"
	ideology = liberalist
}

create_country_leader = {
	name = "Alyaksandr Kazulin"
	picture = "Alyaksandr_Kazulin.dds"
	ideology = social_democrat_ideology
}

create_country_leader = {
	name = "Aleh Novikau"
	picture = "Aleh_Novikau.dds"
	ideology = progressive_ideology
}

create_country_leader = {
	name = "Vladimir Alexandrovich"
	picture = "Vladimir_Alexandrovich.dds"
	ideology = democratic_socialist_ideology
}

create_country_leader = {
	name = "Igor Karpenko"
	picture = "Igor_Karpenko.dds"
	ideology = stalinist
}

add_namespace = {
	name = "blr_unit_leader"
	type = unit_leader
}

create_field_marshal = {
	name = "Andrei Ravkov"
	picture = "generals/Andrei_Ravkov.dds"
	traits = { old_guard organisational_leader }
	skill = 1
}

create_field_marshal = {
	name = "Oleg Belokonev"
	picture = "generals/Oleg_Belokonev.dds"
	traits = { fast_planner }
	skill = 1
}

create_field_marshal = {
	name = "Sergei Potapenko"
	picture = "generals/Sergei_Potapenko.dds"
	traits = { defensive_doctrine }
	skill = 1
}

create_field_marshal = {
	name = "Igor Lotenkov"
	picture = "generals/Igor_Lotenkov.dds"
	traits = { offensive_doctrine }
	skill = 1
}

create_corps_commander = {
	name = "Vitaly Kireyev"
	picture = "generals/Vitaly_Kireyev.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Viktor Hrenin"
	picture = "generals/Viktor_Hrenin.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Ruslan Kosygin"
	picture = "generals/Ruslan_Kosygin.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Andrei Zhuk"
	picture = "generals/Andrei_Zhuk.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = {
	name = "Anatolyi Bulavko"
	picture = "generals/Anatoliy_Bulavko.dds"
	traits = { fortress_buster }
	skill = 1
}

create_corps_commander = {
	name = "Andrei Fedin"
	picture = "generals/Andrei_Fedin.dds"
	traits = { hill_fighter }
	skill = 1
}

create_corps_commander = {
	name = "Oreh Vladimir"
	picture = "generals/Oreh_Vladimir.dds"
	traits = { fortress_buster }
	skill = 1
}

create_corps_commander = {
	name = "Alexander Volfovich"
	picture = "generals/Alexander_Volfovich.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Valeriy Gnilozub"
	picture = "generals/Valery_Gnilozub.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Vladimir Kulazhin"
	picture = "generals/Vladimir_Khulazin.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = {
	name = "Alexander Gurin"
	picture = "generals/Alexander_Gurin.dds"
	traits = { fortress_buster }
	skill = 1
}

create_corps_commander = {
	name = "Igor Kuzmuk"
	picture = "generals/Igor_Kuzmuk.dds"
	traits = { hill_fighter }
	skill = 1
}

create_corps_commander = {
	name = "Vitaly Shkadrovich"
	picture = "generals/Vitaly_Shkadrovich.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = {
	name = "Vadim Denisenko"
	picture = "generals/Vadim_Denisenko.dds"
	traits = { commando urban_assault_specialist }
	skill = 1
}

create_corps_commander = {
	name = "Igor Danilchik"
	picture = "generals/Igor_Danilchik.dds"
	traits = { trickster }
	skill = 1
}

create_corps_commander = {
	name = "Vyacheslav Starkov"
	picture = "generals/Vyacheslav_Starkov.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Dmitry Lukyanenko"
	picture = "generals/Dmitry_Lukyanenko.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Oleg Dvigalev"
	picture = "generals/Oleg_Dvigalev.dds"
	traits = { commando }
	skill = 1
}

create_corps_commander = {
	name = "Sergei Trus"
	picture = "generals/Sergei_Trus.dds"
	traits = { swamp_fox }
	skill = 1
}

create_corps_commander = {
	name = "Igor Golub"
	picture = "generals/Igor_Golub.dds"
	traits = { swamp_fox }
	skill = 1
}

create_corps_commander = {
	name = "Alexander Karev"
	picture = "generals/Alexander_Karev.dds"
	traits = { urban_assault_specialist }
	skill = 1
}

create_corps_commander = {
	name = "Valery Shevchenko"
	picture = "generals/Valery_Shevchenko.dds"
	traits = { trait_mountaineer }
	skill = 1
}

create_corps_commander = {
	name = "Alexander Astrauh"
	picture = "generals/Alexander_Astrauh.dds"
	traits = { bearer_of_artillery }
	skill = 1
}

create_corps_commander = {
	name = "Alexander Panfyorov"
	picture = "generals/Alexander_Panfyorov.dds"
	traits = { winter_specialist }
	skill = 1
}
2017.1.1 = {

oob = "BLR_2017"

set_technology = { 
 legacy_doctrines = 1 
 armoured_mass_assault = 1 
 deep_echelon_advance = 1 
 army_group_operational_freedom = 1 
 massed_artillery = 1
	#Polones MLRS
	gw_artillery = 1
	Arty_upgrade_1 = 1
	Arty_upgrade_2 = 1
	SP_arty_equipment_0 = 1
	SP_arty_equipment_1 = 1
	SP_arty_equipment_2 = 1
	SP_R_arty_equipment_0 = 1
	SP_R_arty_equipment_1 = 1
	SP_R_arty_equipment_2 = 1
	
	night_vision_1 = 1
	night_vision_2 = 1
	
	#For templates
	infantry_weapons = 1
	
	combat_eng_equipment = 1
	command_control_equipment = 1
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	Early_APC = 1
	APC_1 = 1
	Air_APC_1 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	util_vehicle_equipment_0 = 1
	MBT_1 = 1
}

add_ideas = {
	pop_050
	#The_Military
	#The_Intelligence_Community
	#National_Police
	orthodox_christian
	unrestrained_corruption
	gdp_4
	    stagnation
		defence_01
	edu_04
	health_03
	social_03
	bureau_02
	police_05
	partial_draft_army
	volunteer_women
	medium_far_right_movement
	oligarchs
	small_medium_business_owners
	fossil_fuel_industry
	civil_law
}
set_country_flag = gdp_4
set_country_flag = positive_small_medium_business_owners
set_country_flag = positive_fossil_fuel_industry

#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot
	complete_national_focus = Generic_4_IC_slot
	complete_national_focus = Generic_8_IC_slot

	set_politics = {

	parties = {
		democratic = { 
			popularity = 4
		}

		fascism = {
			popularity = 0
		}
		
		communism = {
			popularity = 85
		}
		
		neutrality = {
			popularity = 11
		}
		
		nationalist = {
			popularity = 0
		}
	}
	
	ruling_party = communism
	last_election = "2015.10.11"
	election_frequency = 60
	elections_allowed = yes # none after offset 1
}

create_country_leader = {
	name = "Anatoly Lebedko"
	desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
	picture = "anatoly_lebedko.dds"
	expire = "2065.1.1"
	ideology = liberalism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Tatsiana Karatkevich"
	desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
	picture = "tatsiana_karatkevich.dds"
	expire = "2065.1.1"
	ideology = neutral_Social
	traits = {
		#
	}
}

create_country_leader = {
	name = "Nikolai Ulakhovich"
	desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
	picture = "nikolai_ulakhovich.dds"
	expire = "2065.1.1"
	ideology = Nat_Autocracy
	traits = {
		#
	}
}

create_country_leader = {
	name = "Alexander Lukashenko"
	desc = ""
	picture = "alexander_lukashenko.dds"
	expire = "2065.1.1"
	ideology = Autocracy
	traits = {
		#
	}
}
create_field_marshal = {
	name = "Andrei Ravkov"
	picture = "Portrait_Andrei_Ravkov.dds"
	traits = { old_guard organisational_leader }
	skill = 3
}

create_field_marshal = {
	name = "Oleg Belokonev"
	picture = "Portrait_Oleg_Belokonev.dds"
	traits = { fast_planner }
	skill = 2
}

create_field_marshal = {
	name = "Sergei Potapenko"
	picture = "Portrait_Sergei_Potapenko.dds"
	traits = { defensive_doctrine }
	skill = 2
}

create_field_marshal = {
	name = "Igor Lotenkov"
	picture = "Portrait_Igor_Lotenkov.dds"
	traits = { offensive_doctrine }
	skill = 2
}

create_corps_commander = {
	name = "Vitaly Kireyev"
	picture = "Portrait_Vitaly_Kireyev.dds"
	traits = { trait_engineer }
	skill = 2
}

create_corps_commander = {
	name = "Viktor Hrenin"
	picture = "Portrait_Viktor_Hrenin.dds"
	traits = { panzer_leader }
	skill = 2
}

create_corps_commander = {
	name = "Ruslan Kosygin"
	picture = "Portrait_Ruslan_Kosygin.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Andrei Zhuk"
	picture = "Portrait_Andrei_Zhuk.dds"
	traits = { ranger }
	skill = 2
}

create_corps_commander = {
	name = "Anatolyi Bulavko"
	picture = "Portrait_Anatoliy_Bulavko.dds"
	traits = { fortress_buster }
	skill = 1
}

create_corps_commander = {
	name = "Andrei Fedin"
	picture = "Portrait_Andrei_Fedin.dds"
	traits = { hill_fighter }
	skill = 1
}

create_corps_commander = {
	name = "Oreh Vladimir"
	picture = "Portrait_Oreh_Vladimir.dds"
	traits = { fortress_buster }
	skill = 1
}

create_corps_commander = {
	name = "Alexander Volfovich"
	picture = "Portrait_Alexander_Volfovich.dds"
	traits = { panzer_leader }
	skill = 2
}

create_corps_commander = {
	name = "Valeriy Gnilozub"
	picture = "Portrait_Valery_Gnilozub.dds"
	traits = { trait_engineer }
	skill = 2
}

create_corps_commander = {
	name = "Vladimir Kulazhin"
	picture = "Portrait_Vladimir_Khulazin.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = {
	name = "Alexander Gurin"
	picture = "Portrait_Alexander_Gurin.dds"
	traits = { fortress_buster }
	skill = 1
}

create_corps_commander = {
	name = "Igor Kuzmuk"
	picture = "Portrait_Igor_Kuzmuk.dds"
	traits = { hill_fighter }
	skill = 1
}

create_corps_commander = {
	name = "Vitaly Shkadrovich"
	picture = "Portrait_Vitaly_Shkadrovich.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = {
	name = "Vadim Denisenko"
	picture = "Portrait_Vadim_Denisenko.dds"
	traits = { commando urban_assault_specialist }
	skill = 2
}

create_corps_commander = {
	name = "Igor Danilchik"
	picture = "Portrait_Igor_Danilchik.dds"
	traits = { trickster }
	skill = 1
}

create_corps_commander = {
	name = "Vyacheslav Starkov"
	picture = "Portrait_Vyacheslav_Starkov.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Dmitry Lukyanenko"
	picture = "Portrait_Dmitry_Lukyanenko.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Oleg Dvigalev"
	picture = "Portrait_Oleg_Dvigalev.dds"
	traits = { commando }
	skill = 2
}

create_corps_commander = {
	name = "Sergei Trus"
	picture = "Portrait_Sergei_Trus.dds"
	traits = { swamp_fox }
	skill = 2
}

create_corps_commander = {
	name = "Igor Golub"
	picture = "Portrait_Igor_Golub.dds"
	traits = { swamp_fox }
	skill = 2
}

create_corps_commander = {
	name = "Alexander Karev"
	picture = "Portrait_Alexander_Karev.dds"
	traits = { urban_assault_specialist }
	skill = 2
}

create_corps_commander = {
	name = "Valery Shevchenko"
	picture = "Portrait_Valery_Shevchenko.dds"
	traits = { trait_mountaineer }
	skill = 1
}

create_corps_commander = {
	name = "Alexander Astrauh"
	picture = "Portrait_Alexander_Astrauh.dds"
	traits = { bearer_of_artillery }
	skill = 1
}

create_corps_commander = {
	name = "Alexander Panfyorov"
	picture = "Portrait_Alexander_Panfyorov.dds"
	traits = { winter_specialist }
	skill = 1
}
}
﻿capital = 8

oob = "LUX_2000"

set_convoys = 15
set_stability = 0.5

set_country_flag = country_language_luxembourgish
set_country_flag = country_language_german
set_country_flag = country_language_french

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1

	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_decline
	idea_eu_member
}

set_politics = {

	parties = {
		reactionary = {
			popularity = 11.3
		}
		conservative = {
			popularity = 30.1
		}
		market_liberal = {
			popularity = 22.4
		}
		social_democrat = {
			popularity = 22.3
		}
		progressive = {
			popularity = 9.1
		}
		democratic_socialist = {
			popularity = 3.3
		}
	}
	
	ruling_party = conservative
	last_election = "1999.1.13"
	election_frequency = 60
	elections_allowed = yes
}

create_country_leader = {
	name = "Henri of Luxembourg"
	picture = "Henri_Luxembourg.dds"
	ideology = absolute_monarchist
}

create_country_leader = {
	name = "Robert Mehlen"
	picture = "Robert_Mehlen.dds"
	ideology = counter_progressive_democrat
}

create_country_leader = {
	name = "Jean-Claude Juncker"
	picture = "Jean_Claude_Juncker.dds"
	ideology = christian_democrat
}

create_country_leader = {
	name = "Lydie Polfer"
	picture = "Lydie_Polfer.dds"
	ideology = libertarian
}

create_country_leader = {
	name = "Jean Asselborn"
	picture = "Jean_Asselborn.dds"
	ideology = social_democrat_ideology
}

create_country_leader = {
	name = "Christian Kmiotek"
	picture = "Christian_Kmiotek.dds"
	ideology = green
}

create_country_leader = {
	name = "Fabienne Lentz"
	picture = "Fabienne_Lentz.dds"
	ideology = democratic_socialist_ideology
}

add_namespace = {
	name = "lux_unit_leader"
	type = unit_leader
}

create_field_marshal = {
	name = "Romain Mancinelli"
	picture = "Romain_Mancinelli.dds"
	traits = { old_guard }
	skill = 1
}

create_field_marshal = {
	name = "Gaston Reisig"
	picture = "Gaston_Reinig.dds"
	traits = { fast_planner }
	skill = 1
}

create_field_marshal = {
	name = "Mario Daubenfeld"
	picture = "Mario_Daubenfeld.dds"
	traits = { offensive_doctrine }
	skill = 1
}

create_corps_commander = {
	name = "Alain Duchène"
	picture = "Alain_Duchene.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Marc Assel"
	picture = "Marc_Assel.dds"
	traits = { trickster }
	skill = 1
}

create_corps_commander = {
	name = "Yves Kalmes"
	picture = "Yves_Kalmes.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Claude Schmitz"
	picture = "Claude_Schmitz.dds"
	traits = { ranger }
	skill = 1
}

2002.1.1 = {
	add_ideas = the_euro
}

2004.6.13 = {
	set_politics = {
		last_election = "2004.6.13"
		elections_allowed = yes
		parties = {
			reactionary = {
				popularity = 9.9
			}
			conservative = {
				popularity = 36.1
			}
			market_liberal = {
				popularity = 16.1
			}
			social_democrat = {
				popularity = 23.4
			}
			progressive = {
				popularity = 9.9
			}
			democratic_socialist = {
				popularity = 1.9
			}
			communist = {
				popularity = 0.9
			}
		}
	}
	create_country_leader = {
		name = "Claude Meisch"
		picture = "Claude_Meisch.dds"
		ideology = libertarian
	}

	create_country_leader = {
		name = "Alex Bodry"
		picture = "Alex_Bodry.dds"
		ideology = social_democrat_ideology
	}
	
	create_country_leader = {
		name = "Ali Ruckert"
		picture = "Ali_Ruckert.dds"
		ideology = marxist
	}
}	
2006.1.1 = {
	set_party_name = {
		ideology = reactionary
		long_name = LUX_reactionary_party_ADR_long
		name = LUX_reactionary_party_ADR
	}
}

2009.6.7 = {
	set_politics = {
		last_election = "2009.6.7"
		elections_allowed = yes
		parties = {
			reactionary = {
				popularity = 8.1
			}
			conservative = {
				popularity = 38
			}
			market_liberal = {
				popularity = 15
			}
			social_democrat = {
				popularity = 21.6
			}
			progressive = {
				popularity = 11.7
			}
			democratic_socialist = {
				popularity = 3.3
			}
			communist = {
				popularity = 1.5
			}
		}
	}
}
2013.10.20 = {
	set_politics = {
		ruling_party = market_liberal
		last_election = "2013.10.20"
		elections_allowed = yes
		parties = {
			reactionary = {
				popularity = 6.64
			}
			conservative = {
				popularity = 33.68
			}
			market_liberal = {
				popularity = 18.25
			}
			social_democrat = {
				popularity = 20.28
			}
			progressive = {
				popularity = 10.13
			}
			democratic_socialist = {
				popularity = 4.94
			}
			communist = {
				popularity = 1.64
			}
			social_liberal = {
				popularity = 2.94
			}
		}
	}	

	create_country_leader = {
		name = "Xavier Bettel"
		picture = "Xavier_Bettel.dds"
		ideology = libertarian
	}

	create_country_leader = {
		name = "Claude Haagen"
		picture = "Claude_Haagen.dds"
		expire = "2050.1.1"
	}

	create_country_leader = {
		name = "Sven Clement"
		picture = "Sven_Clement.dds"
		ideology = liberalist
	}
}
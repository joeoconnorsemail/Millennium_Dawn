﻿capital = 96

oob = "LIT_2000"

set_convoys = 100
set_stability = 0.5

set_country_flag = country_language_lithuanian

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	corvette1 = 1
	corvette2 = 1
	frigate_1 = 1
	frigate_2 = 1
	missile_frigate_1 = 1
	missile_frigate_2 = 1
	missile_frigate_2 = 1
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1



	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_decline
	limited_conscription
}

set_politics = {

	parties = {
		nationalist = {
			popularity = 5
		}
		conservative = {
			popularity = 12
		}
		market_liberal = {
			popularity = 2
		}
		social_liberal = {
			popularity = 19
		}
		social_democrat = {
			popularity = 23
		}
		progressive = {
			popularity = 5
		}
		democratic_socialist = {
			popularity = 27
		}
		communist = {
			popularity = 7
		}
	}
	
	ruling_party = conservative
	last_election = "1996.10.20"
	election_frequency = 48
	elections_allowed = yes
}

create_country_leader = {
	name = "Rolandas Paksas"
	picture = "Rolandas_Paksas.dds"
	ideology = democratic_socialist_ideology
}

create_country_leader = {
	name = "Artūras Paulauskas"
	picture = "Arturas_Paulauskas.dds"
	ideology = liberalist
}

create_country_leader = {
	name = "Algirdas Brazauskas"
	picture = "Algirdas_Butkevicius.dds"
	ideology = social_democrat_ideology
}

create_country_leader = {
	name = "Andrius Kubilius"
	picture = "Andrius_Kubilius.dds"
	ideology = christian_democrat
}

create_country_leader = {
	name = "Rapolas Gediminas Sakalnikas"
	picture = "Rapolas_G_Sakalnikas.dds"
	ideology = national_democrat
}

create_country_leader = {
	name = "Mindaugas Gervaldas"
	picture = "Mindaugas_Gervaldas.dds"
	ideology = national_socialist
}

create_country_leader = {
	name = "Romualdas Ozolas"
	picture = "Romualdas_Ozolas.dds"
	ideology = counter_progressive_democrat
}

create_country_leader = {
	name = "Naglis Puteikis"
	picture = "Naglis_Puteikis.dds"
	ideology = libertarian
}

create_country_leader = {
	name = "Ramunas Karbauskis"
	picture = "Ramunas_Karbauskis.dds"
	expire = "2050.1.1"
	ideology = green
}

create_country_leader = {
	name = "Giedrius Petružis"
	picture = "Giedrius_Petruzis.dds"
	expire = "2050.1.1"
	ideology = leninist
}

create_country_leader = {
	name = "Paulius Gediminaitis"
	picture = "Paulius_Gediminatis.dds"
	ideology = absolute_monarchist
}

create_country_leader = {
	name = "Mykolas Rimša"
	picture = "Mykolas_Rimsa.dds"
	ideology = islamic_authoritarian
}

2002.1.1 = {
	add_ideas = the_euro
}

2004.1.1 = { add_ideas = { idea_eu_member } }

2009.1.1 = {
	create_country_leader = {
		name = "Edikas Jagelavičius"
		picture = "Edikas_Jagelavicius.dds"
		ideology = leninist
	}
}

2012.12.1 = {
	set_politics = {
		ruling_party = social_democrat
		last_election = "2012.11.6"
		election_frequency = 48
		elections_allowed = yes
		
		parties = {
			nationalist = {
				popularity = 3
			}
			conservative = {
				popularity = 23
			}
			market_liberal = {
				popularity = 6
			}
			social_liberal = {
				popularity = 21
			}
			social_democrat = {
				popularity = 27
			}
			progressive = {
				popularity = 5
			}
			democratic_socialist = {
				popularity = 10
			}
			communist = {
				popularity = 5
			}
		}
	}
}

2013.1.1 = {
	create_country_leader = {
		name = "Loreta Graužinienė"
		picture = "Loreta_Grauziniene.dds"
		ideology = liberalist
	}
}

2015.1.1 = {
	create_corps_commander = {
		name = "Jonas Vytautas Žukas"
		picture = "generals/Jonas_Vytautas_Zukas.dds"
		skill = 1
	}
}

2015.4.17 = {

	oob = "LIT_2016"
	
	create_country_leader = {
		name = "Gabrielius Landsbergis"
		picture = "Gabrielius_Landsbergis.dds"
		expire = "2050.1.1"
		ideology = christian_democrat
	}
}
﻿capital = 541

oob = "NAM_2000"

set_convoys = 120
set_stability = 0.5

set_country_flag = country_language_english
set_country_flag = country_language_afrikaans

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1
	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_rapid
	african_union_member
	limited_conscription
	commonwealth_of_nations_member
}

set_politics = {

	parties = {
		islamist = { popularity = 0 }
		fascist = { popularity = 0 }
		nationalist = { popularity = 0 }
		reactionary = { popularity = 0 }
		conservative = { popularity = 10 }
		market_liberal = { popularity = 0 }
		social_liberal = { popularity = 3 }
		social_democrat = { popularity = 76 }
		progressive = { popularity = 0 }
		democratic_socialist = { popularity = 10 }
		communist = { popularity = 1 }
	}
	
	ruling_party = social_democrat
	last_election = "1999.12.1"
	election_frequency = 60
	elections_allowed = yes
}

create_country_leader = {
	name = "Sam Nujoma"
	picture = "Sam_Nujoma.dds"
	ideology = social_democrat_ideology
}

create_country_leader = {
	name = "Ben Ulenga"
	picture = "Ben_Ulenga.dds"
	ideology = democratic_socialist_ideology
}

create_country_leader = {
	name = "Katuutire Kaura"
	picture = "Katuutire_Kaura.dds"
	ideology = constitutionalist
}

create_country_leader = {
	name = "Chief Justus Garoeb"
	picture = "Justus_Garoeb.dds"
	ideology = moderate
}

create_country_leader = {
	name = "Attie Beukes"
	picture = "Attie_Beukes.dds"
	ideology = marxist
}

add_namespace = {
	name = "nam_unit_leader"
	type = unit_leader
}

create_field_marshal = {
	name = "Epaphras Denga Ndaitwah"
	picture = "generals/Epaphras_Ndaitwah.dds"
	traits = { old_guard }
	skill = 2
}

create_field_marshal = {
	name = "John Mutwa"
	picture = "generals/John_Mutwa.dds"
	traits = { old_guard }
	skill = 1
}

create_corps_commander = {
	name = "Martin Shally"
	picture = "generals/Martin_Shally.dds"
	traits = { old_guard }
	skill = 1
}

create_corps_commander = {
	name = "Nghilifavali N. Hamunyela"
	picture = "generals/Nghilifavali_Hamunyela.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = {
	name = "Martin Pinehas"
	picture = "generals/Martin_Pinehas.dds"
	traits = { commando }
	skill = 1
}

create_corps_commander = {
	name = "Sabastian Ndeitunga"
	picture = "generals/Sabastian_Ndeitunga.dds"
	traits = { urban_assault_specialist }
	skill = 1
}
	
create_navy_leader = {
	name = "Peter Vilho"
	picture = "admirals/Peter_Vilho.dds"
	skill = 2
}

2014.11.28 = {
	set_politics = {
		parties = {
			islamist = { popularity = 0 }
			fascist = { popularity = 0 }
			nationalist = { popularity = 0 }
			reactionary = { popularity = 0 }
			conservative = { popularity = 5 }
			market_liberal = { popularity = 0 }
			social_liberal = { popularity = 5 }
			social_democrat = { popularity = 81 }
			progressive = { popularity = 5 }
			democratic_socialist = { popularity = 1 }
			communist = { popularity = 3 }
		}
		ruling_party = social_democrat
		last_election = "2014.11.28"
		election_frequency = 60
		elections_allowed = yes
	}
	create_country_leader = {
		name = "Hage Geingob"
		picture = "Hage_Geingob.dds"
		ideology = social_democrat_ideology
	}
	create_country_leader = {
		name = "McHenry Venaani"
		picture = "McHenry_Venaani.dds"
		ideology = fiscal_conservative
	}
	create_country_leader = {
		name = "Hidipo Hamutenya"
		picture = "Hidipo_Hamutenya.dds"
		ideology = progressive_ideology
	}
}
﻿capital = 545

oob = "ZAM_2000"

set_convoys = 10
set_stability = 0.5

set_country_flag = country_language_english

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1

	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
	
}

add_ideas = {
	population_growth_explosion
	african_union_member
	commonwealth_of_nations_member
}

set_politics = {

	parties = {
	islamist = {
	popularity = 0
	}
	fascist = {
	popularity = 0
	}
	nationalist = {
	popularity = 0
	}
	reactionary = {
	popularity = 0
	}
	conservative = {
	popularity = 0
	}
	market_liberal = {
	popularity = 0
	}
	social_liberal = {
	popularity = 25
	}
	social_democrat = {
	popularity = 75
	}
	progressive = {
	popularity = 0
	}
	democratic_socialist = {
	popularity = 0
	}
	communist = {
	popularity = 0
	}
	}
	
	ruling_party = social_democrat
	last_election = "1996.11.18"
	election_frequency = 60
	elections_allowed = yes
}
	
create_country_leader = {
	name = "Frederick Chiluba"
	picture = "Frederick_Chiluba.dds"
	ideology = social_democrat_ideology
}

create_country_leader = {
	name = "Hakainde Hichilema"
	picture = "Hakainde_Hichilema.dds"
	ideology = liberalist
}

create_country_leader = {
	name = "Michael Sata"
	picture = "Michael_Sata.dds"
	ideology = democratic_socialist_ideology
}

create_country_leader = {
	name = "Elias Chipimo Jr."
	picture = "Elias_Chipimo.dds"
	ideology = fiscal_conservative
}

create_country_leader = {
	name = "Godfrey Miyanda"
	picture = "Godfrey_Miyanda.dds"
	ideology = counter_progressive_democrat
}

create_corps_commander = {
	name = "Nathan Mulenga"
	picture = "generals/Nathan_Mulenga.dds"
	skill = 2
}

2005.1.1 = {
	set_party_name = {
		ideology = social_liberal
		long_name = ZAM_social_liberal_party_UPND_long
		name = ZAM_social_liberal_party_UPND
	}
}

2011.9.20 = {
	
	set_politics = {

	parties = {
	islamist = {
	popularity = 0
	}
	fascist = {
	popularity = 0
	}
	nationalist = {
	popularity = 0
	}
	reactionary = {
	popularity = 0
	}
	conservative = {
	popularity = 0
	}
	market_liberal = {
	popularity = 0
	}
	social_liberal = {
	popularity = 20
	}
	social_democrat = {
	popularity = 35
	}
	progressive = {
	popularity = 0
	}
	democratic_socialist = {
	popularity = 45
	}
	communist = {
	popularity = 0
	}
	}
	
	ruling_party = democratic_socialist
	last_election = "1996.11.18"
	election_frequency = 60
	elections_allowed = yes
	}

	create_country_leader = {
		name = "Rupiah Banda"
		picture = "Rupiah_Banda.dds"
		ideology = social_democrat_ideology
	}

}

2014.10.29 = {
	create_country_leader = {
		name = "Guy Scott"
		picture = "Guy_Scott.dds"
		ideology = democratic_socialist_ideology
	}
}

2015.1.25 = {
	create_country_leader = {
		name = "Edgar Lungu"
		picture = "Edgar_Lungu.dds"
		ideology = democratic_socialist_ideology
	}
}
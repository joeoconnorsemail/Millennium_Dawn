﻿capital = 671

oob = "VIN_2000"

set_convoys = 600
set_stability = 0.5

set_country_flag = country_language_vietnamese

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	AT_upgrade_1 = 1
	Heavy_Anti_tank_1 = 1
	Anti_tank_1 = 1 
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1
	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	

}

add_ideas = {
	population_growth_rapid
	limited_conscription
}

add_opinion_modifier = {
	target = JAP
	modifier = past_japanese_war_crimes
}

set_politics = {

	parties = {
		islamist = {
			popularity = 0
		}
		nationalist = {
			popularity = 5
		}
		reactionary = {
			popularity = 0
		}
		conservative = {
			popularity = 5
		}
		market_liberal = {
			popularity = 0
		}
		social_liberal = {
			popularity = 5
		}
		social_democrat = {
			popularity = 0
		}
		progressive = {
			popularity = 0
		}
		democratic_socialist = {
			popularity = 5
		}
		communist = {
			popularity = 80
		}
	}
	
	ruling_party = communist
	last_election = "1997.1.1"
	election_frequency = 48
	elections_allowed = no
}

2000.1.1 = {
	create_country_leader = {
		name = "Tran Duc Luong"
		picture = "Tran_Duc_Luong.dds"
		ideology = leninist
	}

	create_country_leader = {
		name = "Thadeus Nguyen Van Ly"
		picture = "Nguyen_Thien_Nhan.dds"
		ideology = liberalist
	}

	create_country_leader = {
		name = "Pham The Duyet"
		picture = "Pham_The_Duyet"
		ideology = national_democrat
	}

	create_country_leader = {
		name = "Do Hoang Diem"
		picture = "Do_Hoang_Diem"
		ideology = constitutionalist
	}

	create_corps_commander = {
		name = "Phan Van Giang"
		picture = "generals/Phan_Van_Giang.dds"
		skill = 2
	}

	create_corps_commander = {
		name = "Do Ba Ty"
		picture = "generals/Do_Ba_Ty.dds"
		skill = 1
	}

	create_corps_commander = {
		name = "Ngo Xuan Lich"
		picture = "generals/Ngo_Xuan_Lich.dds"
		skill = 1
	}

	create_corps_commander = {
		name = "Phung Quang Thanh"
		picture = "generals/Phung_Quang_Thanh.dds"
		skill = 1
	}

	create_navy_leader = {
		name = "Pham Hoai Nam"
		picture = "admirals/Pham_Hoai_Nam.dds"
		skill = 2
	}

}

2006.6.27 = {
	create_country_leader = {
		name = "Nguyen Minh Triet"
		picture = "Nguyen_Minh_Triet.dds"
		ideology = leninist
	}
}

2011.7.25 = {
	create_country_leader = {
		name = "Truong Tan Sang"
		picture = "Truong_Tan_Sang.dds"
		ideology = leninist
	}

	create_country_leader = {
		name = "Vi Duc Hoi"
		picture = "Vi_Duc_Hoi.dds"
		ideology = liberalist
	}

	create_country_leader = {
		name = "Nguyen Thien Nhan"
		picture = "Nguyen_Thien_Nhan.dds"
		ideology = national_democrat
	}
}

2016.4.2 = {
	create_country_leader = {
		name = "Tran Dai Quang"
		picture = "Tran_Dai_Quang.dds"
		ideology = leninist
	}
}
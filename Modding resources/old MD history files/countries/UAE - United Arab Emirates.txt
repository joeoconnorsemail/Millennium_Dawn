﻿capital = 658

oob = "UAE_2000"

set_convoys = 500
set_stability = 0.5

set_war_support = 0.2

set_country_flag = country_language_arabic

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1
	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

set_country_flag = dominant_religion_islam
set_country_flag = sunni_islam

add_ideas = {
	population_growth_rapid
	arab_league_member
	limited_conscription
}

set_politics = {

	parties = {
		islamist = {
			popularity = 30
		}
		conservative = {
			popularity = 5
		}
		monarchist = {
			popularity = 60
		}
		social_liberal = {
			popularity = 5
		}
	}
	
	ruling_party = monarchist
	last_election = "1997.1.1"
	election_frequency = 48
	elections_allowed = no
}

create_country_leader = {
	name = "Khalifa bin Zayed"
	picture = "Khalifa_Bin_Zayed.dds"
	ideology = absolute_monarchist
}
create_country_leader = {
	name = "Sultan bin Kayed Al-Qasimi"
	ideology = islamic_authoritarian
	picture = "Sultan_Al_Qasimi.dds" 
}
create_country_leader = {
	name = "Khaled Al-Dakhil"
	ideology = national_democrat
	picture = "Khaled_Al-Dakhil.dds"
}

create_country_leader = {
	name = "Nasir bin Ghaith"
	picture = "Nasir_bin_Ghaith.dds"
	ideology = libertarian
}

create_country_leader = {
	name = "Ibrahim Al Haram"
	picture = "Ibrahim_Al_Haram.dds"
	ideology = counter_progressive_democrat
}
create_country_leader = {
	name = "Abdul Khaliq Abdullah"
	picture = "Abdul_Khaliq_Abdullah.dds"
	ideology = liberalist
}
create_country_leader = {
	name = "Sultan Saud Al-Qasimi"
	picture = "Sultan_S_Qasimi.dds"
	ideology = social_democrat_ideology
}
create_country_leader = {
	name = "Ahmed Al Jarwan"
	picture = "Ahmed_Al_Jarwan.dds"
	ideology = constitutionalist
}
create_country_leader = {
	name = "Khaled Al Qasimi"
	picture = "Khaled_Al_Qasimi.dds"
	ideology = marxist
}
create_country_leader = {
	name = "Sultan bin Mohammed Al-Qasimi"
	picture = "Sultan_M_Qasimi.dds"
	ideology = national_socialist
}
create_country_leader = {
	name = "Sa'adat Jamal Al Zaabi"
	picture = "Saadat_J_Zaabi.dds"
	ideology = democratic_socialist_ideology
}
create_country_leader = {
	name = "Louay Mohammad Deeb"
	picture = "Louay_M_Deeb.dds"
	ideology = progressive_ideology
}

create_field_marshal = {
	name = "Hamad Mohammed Al-Rumaithi"
	picture = "Hamad_Mohammed_Al-Rumaithi.dds"
	skill = 2
}
create_corps_commander = {
	name = "Gomaa Ahmed Al-Bawardi"
	picture = "Gomaa_A_Bawardi.dds"
	skill = 2
}

create_corps_commander = {
	name = "Saleh Mohammed Al-Ameri"
	picture = "Saleh_M_Ameri.dds"
	skill = 1
}

create_corps_commander = {
	name = "Ahmed Bin Tahnoon"
	picture = "Ahmed_Tahnoon.dds"
	skill = 2
}

create_corps_commander = {
	name = "Issa Saif Al-Mazrouei"
	picture = "Issa_S_Mazrouei.dds"
	skill = 1
}

create_corps_commander = {
	name = "Al Matrooshi Mourn"
	picture = "Al_Matrooshi_Mourn.dds"
	skill = 1
}

create_corps_commander = {
	name = "Khalifa bin Nahila"
	picture = "Khalifa_Nahila.dds"
	skill = 2
}

create_corps_commander = {
	name = "Mohammed Al Dhahiri"
	picture = "Mohammed_Al_Dhahiri.dds"
	skill = 1
}

create_corps_commander = {
	name = "Abdul Qudous Abdul Razzaq"
	picture = "Abdul_Qudous_Abdul_Razzaq.dds"
	skill = 1
}

create_corps_commander = {
	name = "Hamad Adil Al-Shamsi"
	picture = "Hamad_A_AlShamsi.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = {
	name = "Mubarak bin Muhairoum"
	picture = "Mubarak_Muhairoum.dds"
	traits = { trickster }
	skill = 1
}

create_corps_commander = {
	name = "Mubarak Abdullah Al-Muhairi"
	picture = "Al_Muhairi.dds"
	traits = { bearer_of_artillery }
	skill = 1
}

create_navy_leader = {
	name = "Ibrahim Salem Mohammed"
	picture = "Ib_S_Mohammed.dds"
	traits = { old_guard_navy superior_tactician }
	skill = 1
}
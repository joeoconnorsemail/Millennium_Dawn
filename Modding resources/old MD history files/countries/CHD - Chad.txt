﻿capital = 752

oob = "CHD_2000"

set_convoys = 10
set_stability = 0.5

set_country_flag = country_language_arabic
set_country_flag = country_language_french

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1

	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_explosion
	african_union_member
}

set_country_flag = dominant_religion_islam
set_country_flag = sunni_islam

set_politics = {

	parties = {
		islamist = {
			popularity = 4
		}
		reactionary = {
			popularity = 40
		}
		conservative = {
			popularity = 10
		}
		social_liberal = {
			popularity = 10
		}
		democratic_socialist = {
			popularity = 20
		}
		communist = {
			popularity = 6
		}
	}
	
	ruling_party = reactionary
	last_election = "1997.1.1"
	election_frequency = 48
	elections_allowed = no
}

create_country_leader = {
	name = "Idriss Déby"
	ideology = oligarchist
	picture = "Idriss_Deby.dds"
}
2017.1.1 = {

oob = "CHA_2017"

# Starting tech
set_technology = { 
 legacy_doctrines = 1 
 modern_blitzkrieg = 1 
 forward_defense = 1 
 encourage_nco_iniative = 1 
 air_land_battle = 1
	#For templates
	infantry_weapons = 1
	combat_eng_equipment = 1
	command_control_equipment = 1
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	gw_artillery = 1
	SP_arty_equipment_0 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	Early_APC = 1
	APC_1 = 1
	IFV_1 = 1
	MBT_1 = 1
	util_vehicle_equipment_0 = 1
	Rec_tank_0 = 1
}

add_ideas = {
	pop_050
	rampant_corruption
	gdp_1
	sufi_islam
		stagnation
		defence_03
	edu_01
	health_01
	social_01
	bureau_01
	police_04
	rentier_state
    export_economy
	draft_army
	drafted_women
	Enduring_Freedom
	farmers
	fossil_fuel_industry
	the_military
	hybrid
}
set_country_flag = gdp_1
set_country_flag = negative_farmers
set_country_flag = enthusiastic_fossil_fuel_industry
set_country_flag = positive_the_military

#Nat focus
	complete_national_focus = bonus_tech_slots

set_politics = {

	parties = {
		democratic = { 
			popularity = 0
		}

		fascism = {
			popularity = 5
		}
		
		communism = {
			popularity = 0
			#banned = no #default is no
		}
		
		neutrality = { 
			popularity = 100
		}
	}
	
	ruling_party = neutrality
	last_election = "2016.4.10"
	election_frequency = 60
	elections_allowed = yes
}

create_country_leader = {
	name = "Idriss Deby"
	desc = "POLITICS_Mohamed Cheikh Biadillah_DESC" 
	picture = "CHA_Idriss_Deby.dds"
	expire = "2050.1.1"
	ideology = Neutral_conservatism
	traits = {
		#
	}
}
create_field_marshal = {
	name = "Oumar Bikimo"
	picture = "generals/Portrait_Oumar_Bikimo.dds"
	traits = { old_guard inspirational_leader }
	skill = 3
}

create_field_marshal = {
	name = "Mahamat Brahim Seid"
	picture = "generals/Portrait_Mahamat_Brahim_Seid.dds"
	traits = { organisational_leader }
	skill = 2
}

create_field_marshal = {
	name = "Benaindo Tatola"
	picture = "generals/Portrait_Benaindo_Tatola.dds"
	traits = { defensive_doctrine }
	skill = 1
}

create_corps_commander = {
	name = "Mahamat Idriss Déby"
	picture = "generals/Portrait_Mahamat_Ibriss_Deby.dds"
	traits = { trait_engineer }
	skill = 2
}

create_corps_commander = {
	name = "Yaya Daoud Brahim"
	picture = "generals/Portrait_Yaya_Daoud_Brahim.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = {
	name = "Taher Erda"
	picture = "generals/Portrait_Taher_Erda.dds"
	traits = { urban_assault_specialist }
	skill = 1
}

create_corps_commander = {
	name = "G.A.L. Tahir"
	picture = "generals/Portrait_GAL_Tahir.dds"
	traits = { fortress_buster }
	skill = 1
}

create_navy_leader = {
	name = "Oumar Bikimo"
	picture = "admirals/Portrait_Oumar_Bikimo.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Mahamat Brahim Seid"
	picture = "admirals/Portrait_Mahamat_Brahim_Seid.dds"
	traits = {  }
	skill = 1
}
}
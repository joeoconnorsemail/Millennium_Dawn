﻿#This is the Czech Republic. Ignore the file name.#

capital = 9

oob = "CZE_2000"

set_convoys = 10
set_stability = 0.5

set_country_flag = country_language_czech

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1

	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_stagnation
	visegrad_group_member
	free_trade
}

set_politics = {
	parties = {
		islamist = { popularity = 0 }
		fascist = { popularity = 4 }
		nationalist = { popularity = 0 }
		reactionary = { popularity = 0 }
		conservative = { popularity = 13 }
		market_liberal = { popularity = 0 }
		social_liberal = { popularity = 31 }
		social_democrat = { popularity = 40 }
		progressive = { popularity = 1 }
		democratic_socialist = { popularity = 0 }
		communist = { popularity = 11 }
	}
	ruling_party = social_democrat
	last_election = "1998.6.20"
	election_frequency = 48
	elections_allowed = yes
}

add_opinion_modifier = {
	target = HUN
	modifier = visegrad_group
}
add_opinion_modifier = {
	target = POL
	modifier = visegrad_group
}
add_opinion_modifier = {
	target = SLO
	modifier = visegrad_group
}
	
#create_country_leader = {
#	name = "Milos Zeman"
#	picture = "Milos_Zeman.dds"
#	ideology = social_democrat_ideology
#}

create_country_leader = {
	name = "Vaclav Havel"
	picture = "Vaclav_Havel.dds"
	ideology = social_democrat_ideology
}

create_country_leader = {
	name = "Vaclav Klaus"
	picture = "Vaclav_Klaus.dds"
	ideology = moderate
}

create_country_leader = {
	name = "Miroslav Grebenicek"
	picture = "Miroslav_Grebenicek.dds"
	ideology = marxist
}

create_country_leader = {
	name = "Josef Lux"
	picture = "Josef_Lux.dds"
	ideology = christian_democrat
}

create_country_leader = {
	name = "Miroslav Sladek"
	picture = "Miroslav_Sladek.dds"
	ideology = national_socialist
}

create_country_leader = {
	name = "Petr Michek"
	picture = "Petr_Michek.dds"
	ideology = proto_fascist
}

create_country_leader = {
	name = "Matej Stropnicky"
	picture = "Matej_Stropicky.dds"
	ideology = green
}

create_corps_commander = {
	name = "Josef Becvar"
	picture = "generals/Josef_Becvar.dds"
	skill = 2
}

create_corps_commander = {
	name = "Miroslav Zizka"
	picture = "generals/Miroslav_Zizka.dds"
	skill = 1
}

create_corps_commander = {
	name = "Ales Opata"
	picture = "generals/Ales_Opata.dds"
	skill = 1
}

create_corps_commander = {
	name = "Frantisek Maleninsky"
	picture = "generals/Frantisek_Maleninsky.dds"
	skill = 1
}



2004.1.1 = {
	add_ideas = {
		idea_eu_member
	}
}

2009.1.1 = {
	
	set_party_name = {
		ideology = conservative
		name = CZE_conservative_party_TOP09
	}

}

2013.10.26 = {
	set_politics = {
		parties = {
			islamist = { popularity = 0 }
			fascist = { popularity = 0 }
			nationalist = { popularity = 0 }
			reactionary = { popularity = 7 }
			conservative = { popularity = 20 }
			market_liberal = { popularity = 20 }
			social_liberal = { popularity = 10 }
			social_democrat = { popularity = 20 }
			progressive = { popularity = 8 }
			democratic_socialist = { popularity = 0 }
			communist = { popularity = 15 }
		}
		ruling_party = social_democrat
		last_election = "2013.10.26"
		election_frequency = 48
		elections_allowed = yes
	}
	create_country_leader = {
		name = "Andrej Babis"
		picture = "Andrej_Babis.dds"
		ideology = libertarian
	}
	create_country_leader = {
		name = "Bohuslav Sobotka"
		picture = "Bohuslav_Sobotka.dds"
		ideology = social_democrat_ideology
	}
	create_country_leader = {
		name = "Vojtech Filip"
		picture = "Vojtech_Filip.dds"
		ideology = marxist
	}
	create_country_leader = {
		name = "Karel Schwarzenberg"
		picture = "Karel_Schwarzenberg.dds"
		ideology = fiscal_conservative
	}
	create_country_leader = {
		name = "Miroslava Nemcova"
		picture = "Miroslava_Nemcova.dds"
		ideology = centrist
	}
	create_country_leader = {
		name = "Miroslav Lidinsky"
		picture = "Miroslav_Lidinsky.dds"
		ideology = counter_progressive_democrat
	}
}
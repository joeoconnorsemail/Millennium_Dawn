scripted_diplomatic_actions = {
	embargo_diplomatic_action = { # will be used in most localizations
		# root is the initiator of action and this is the target country
		allowed = { #if action is valid
			ROOT = { THIS = { NOT = { has_country_flag = md_embargoed@ROOT } } }
		}
		visible = { #if action is visible on gui
			always = yes
		}
		selectable = { #if action is selectable on gui
			always = yes
		}
		requires_acceptance = no # if true the action will require acceptance of target country
		
		cost = 50 # pp cost, can be zero
		command_power = 5 # cp cost, can be zero

		#cost_string = loc_key # if you have a custom cost, use this loc to display it
		
		show_acceptance_on_action_button = yes # set to no if acceptance shouldn't be displayed on the action button
		icon = 1 # icon frame to use in notification

		#for effects/triggers/variables, root is the sender and this is receiver
		# runs when the action is sent
		on_sent_effect = {
			ROOT = {
				add_opinion_modifier = {
					target = THIS
					modifier = MD_embargo
				}
				THIS = { set_country_flag = md_embargoed@ROOT }
			}
		}

		# runs when the action is properly sent and accepted
		complete_effect = {
			effect_tooltip = { 
				ROOT = {
					add_opinion_modifier = {
						target = THIS
						modifier = MD_embargo
					}
					THIS = { set_country_flag = md_embargoed@ROOT }
				}
			}
		}
		
		# SEND POPUP CUSTOMIZATION
		# use diplomatic_action as context in scripted gui
		# send_scripted_gui = send_scripted_gui_name # if specified, the diplomatic action will use this scripted gui before sending the request. the player (or ai) can make changes on that gui and store stuff as variable to customize action
		
		#reset_send_effect = { # if action has a send scripted gui, this effect will be used for clearing the state of gui (will run when player opens the scripted gui by sending the action or when ai proposes action
			# clear action
			#if = {
				#limit = { 
					#root = {
						#is_ai = yes
					#}
				#}
				# initialize for ai
			#}
		#}
		
		can_be_sent = { # send button will be disabled if no
			always = yes
		}
		
		send_description = embargo_send # if there is no scripted gui, this desc will be used in send yes/no pop up
		
		# RECEIVE POPUP CUSTOMIZATION
		# use diplomatic_action as context in scripted gui
		# receive_scripted_gui = receive_scripted_gui_name #if specified the receiver of the action will get this scripted gui instead of yes-no popup when they receive the propose
		#reset_receive_effect = {
			# clear action
			#if = {
				#limit = { 
					#is_ai = yes
				#}
				# change stuff for ai
			#}
		#}
		
		can_be_accepted = { # accept button will be disabled if no
			always = yes
		}
		
		receive_description = embargo_receive # if there is no scripted gui, this desc will be used in send yes/no pop up
		
		# PLAYER FEEDBACK ON ACCEPT/REJECT
		accept_title = embargo_accept #can be overridden to customize accepted/rejected pop ups for the sender
		accept_description = embargo_accept_desc
		reject_title = embargo_reject
		reject_description = embargo_reject_desc
		
		# AI
		# a list of ai_will_do entries that will be used to decide if AI should accept or reject a request
		ai_acceptance = {
			condition = { # name is used for loc
				base = 100
			}
		}

		# ai_will_do to decide if AI should send this action or not
		ai_desire = {
			base = -1
			modifier = {
				ROOT = {
					has_opinion = {
						target = THIS
						value = -100
					}
				}
			}
		}
	}
}
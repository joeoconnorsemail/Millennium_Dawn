#	Example:
#
#	example_effect = {
#		add_political_power = 66
#		add_popularity = {
#			ideology = fascism
#			popularity = 0.33
#		}
#	}
#
#
#	In a script file:
#
#	effect = {
#		example_effect = yes
#	}
#

set_crime_fighting_flags = {
	if = {
		limit = { has_idea = paralyzing_corruption }
		set_country_flag = corruption_10
	}
	if = {
		limit = { has_idea = crippling_corruption }
		set_country_flag = corruption_9
	}
	if = {
		limit = { has_idea = rampant_corruption }
		set_country_flag = corruption_8
	}
	if = {
		limit = { has_idea = unrestrained_corruption }
		set_country_flag = corruption_7
	}
	if = {
		limit = { has_idea = systematic_corruption }
		set_country_flag = corruption_6
	}
	if = {
		limit = { has_idea = widespread_corruption }
		set_country_flag = corruption_5
	}
	if = {
		limit = { has_idea = medium_corruption }
		set_country_flag = corruption_4
	}
	if = {
		limit = { has_idea = modest_corruption }
		set_country_flag = corruption_3
	}
	if = {
		limit = { has_idea = slight_corruption }
		set_country_flag = corruption_2
	}
	if = {
		limit = { has_idea = negligible_corruption }
		set_country_flag = corruption_1
	}

}

set_economic_cycle_flags = {

	if = {
		limit = { has_idea = depression }
		set_country_flag = economic_cycle_6
	}
	if = {
		limit = { has_idea = recession }
		set_country_flag = economic_cycle_5
	}
	if = {
		limit = { has_idea = stagnation }
		set_country_flag = economic_cycle_4
	}
	if = {
		limit = { has_idea = stable_growth }
		set_country_flag = economic_cycle_3
	}
	if = {
		limit = { has_idea = fast_growth }
		set_country_flag = economic_cycle_2
	}
	if = {
		limit = { has_idea = economic_boom }
		set_country_flag = economic_cycle_1
	}
}

set_growth_flag = {
	if = {
		limit = { has_idea = depression }
		set_country_flag = economic_cycle_6
		clr_country_flag = economic_cycle_5
		clr_country_flag = economic_cycle_4
		clr_country_flag = economic_cycle_3
		clr_country_flag = economic_cycle_2
		clr_country_flag = economic_cycle_1
	}
	if = {
		limit = { has_idea = recession }
			clr_country_flag = economic_cycle_6
			set_country_flag = economic_cycle_5
			clr_country_flag = economic_cycle_4
			clr_country_flag = economic_cycle_3
			clr_country_flag = economic_cycle_2
			clr_country_flag = economic_cycle_1
	}
	if = {
		limit = { has_idea = stagnation }
			clr_country_flag = economic_cycle_6
			clr_country_flag = economic_cycle_5
			set_country_flag = economic_cycle_4
			clr_country_flag = economic_cycle_3
			clr_country_flag = economic_cycle_2
			clr_country_flag = economic_cycle_1
	}
	if = {
		limit = { has_idea = stable_growth }
			clr_country_flag = economic_cycle_6
				clr_country_flag = economic_cycle_5
				clr_country_flag = economic_cycle_4
				set_country_flag = economic_cycle_3
				clr_country_flag = economic_cycle_2
				clr_country_flag = economic_cycle_1
	}
	if = {
		limit = { has_idea = fast_growth }
		clr_country_flag = economic_cycle_6
				clr_country_flag = economic_cycle_5
				clr_country_flag = economic_cycle_4
				clr_country_flag = economic_cycle_3
				set_country_flag = economic_cycle_2
				clr_country_flag = economic_cycle_1
	}
	if = {
		limit = { has_idea = economic_boom }
		clr_country_flag = economic_cycle_6
				clr_country_flag = economic_cycle_5
				clr_country_flag = economic_cycle_4
				clr_country_flag = economic_cycle_3
				clr_country_flag = economic_cycle_2
				set_country_flag = economic_cycle_1
	}
}

Update_GDPC = {
	if = { #Swap GDP_1 for GDP_2
		limit = {
			has_idea = gdp_1
			OR = {
				AND = {
					num_of_factories < 79
					no_building_slots = yes
				}
				AND = {
					num_of_factories > 78
					max_1_building_slots = yes
				}
			}
		}
		swap_ideas = { remove_idea = gdp_1 add_idea = gdp_2 }
		custom_effect_tooltip = gdp_2_TT
	}
	if = { ##Swap GDP_2 for GDP_3
		limit = {
			has_idea = gdp_2
			OR = {
				AND = {
					num_of_factories < 79
					no_building_slots = yes
				}
				AND = {
					num_of_factories > 78
					max_1_building_slots = yes
				}
			}
		}
		swap_ideas = { remove_idea = gdp_2 add_idea = gdp_3 }
		custom_effect_tooltip = gdp_3_TT
	}
	if = {
		limit = {
			has_idea = gdp_3
			OR = {
				AND = {
					num_of_factories < 79
					no_building_slots = yes
				}
				AND = {
					num_of_factories > 78
					max_1_building_slots = yes
				}
			}
		}
		swap_ideas = { remove_idea = gdp_3 add_idea = gdp_4 }
		custom_effect_tooltip = gdp_4_TT
	}
	if = {
		limit = {
			has_idea = gdp_4
			OR = {
				AND = {
					num_of_factories < 79
					no_building_slots = yes
				}
				AND = {
					num_of_factories > 78
					max_1_building_slots = yes
				}
			}
		}
		swap_ideas = { remove_idea = gdp_4 add_idea = gdp_5 }
		custom_effect_tooltip = gdp_5_TT
	}
	if = {
		limit = {
			has_idea = gdp_5
			OR = {
				AND = {
					num_of_factories < 79
					no_building_slots = yes
				}
				AND = {
					num_of_factories > 78
					max_1_building_slots = yes
				}
			}
		}
		swap_ideas = { remove_idea = gdp_5 add_idea = gdp_6 }
		custom_effect_tooltip = gdp_6_TT
	}
	if = {
		limit = {
			has_idea = gdp_6
			OR = {
				AND = {
					num_of_factories < 79
					no_building_slots = yes
				}
				AND = {
					num_of_factories > 78
					max_1_building_slots = yes
				}
			}
		}
		swap_ideas = { remove_idea = gdp_6 add_idea = gdp_7 }
		custom_effect_tooltip = gdp_7_TT
	}
	if = {
		limit = {
			has_idea = gdp_7
			OR = {
				AND = {
					num_of_factories < 79
					no_building_slots = yes
				}
				AND = {
					num_of_factories > 78
					max_1_building_slots = yes
				}
			}
		}
		swap_ideas = { remove_idea = gdp_7 add_idea = gdp_8 }
		custom_effect_tooltip = gdp_8_TT
	}
	if = {
		limit = {
			has_idea = gdp_8
			OR = {
				AND = {
					num_of_factories < 79
					no_building_slots = yes
				}
				AND = {
					num_of_factories > 78
					max_1_building_slots = yes
				}
			}
		}
		swap_ideas = { remove_idea = gdp_8 add_idea = gdp_9 }
		custom_effect_tooltip = gdp_9_TT
	}
	if = {
		limit = {
			has_idea = gdp_9
			OR = {
				AND = {
					num_of_factories < 79
					no_building_slots = yes
				}
				AND = {
					num_of_factories > 78
					max_1_building_slots = yes
				}
			}
		}
		swap_ideas = { remove_idea = gdp_9 add_idea = gdp_10 }
		custom_effect_tooltip = gdp_10_TT
	}
	if = {
		limit = {
			has_idea = gdp_10
			OR = {
				AND = {
					num_of_factories < 79
					no_building_slots = yes
				}
				AND = {
					num_of_factories > 78
					max_1_building_slots = yes
				}
			}
		}
		swap_ideas = { remove_idea = gdp_10 add_idea = gdp_11 }
		custom_effect_tooltip = gdp_11_TT
	}
	if = {
		limit = {
			has_idea = gdp_11
			OR = {
				AND = {
					num_of_factories < 79
					no_building_slots = yes
				}
				AND = {
					num_of_factories > 78
					max_1_building_slots = yes
				}
			}
		}
		swap_ideas = { remove_idea = gdp_11 add_idea = gdp_12 }
		custom_effect_tooltip = gdp_12_TT
		set_country_flag = Max_GDPC
	}
}

##Random Construction -- Please do not forget to include actual costs to accompany these
one_random_arms_factory = {
	random_owned_controlled_state = {
		limit = {
			free_building_slots = {
				building = arms_factory
				size > 0
				include_locked = yes
			}
			OR = {
				is_in_home_area = yes
				NOT = {
					owner = {
						any_owned_state = {
							free_building_slots = {
								building = arms_factory
								size > 0
								include_locked = yes
							}
							is_in_home_area = yes
						}
					}
				}
			}
		}
		add_extra_state_shared_building_slots = 1
		add_building_construction = {
			type = arms_factory
			level = 1
			instant_build = yes
		}
	}
}
two_random_arms_factory = {
	random_owned_controlled_state = {
		limit = {
			free_building_slots = {
				building = arms_factory
				size > 0
				include_locked = yes
			}
			OR = {
				is_in_home_area = yes
				NOT = {
					owner = {
						any_owned_state = {
							free_building_slots = {
								building = arms_factory
								size > 0
								include_locked = yes
							}
							is_in_home_area = yes
						}
					}
				}
			}
		}
		add_extra_state_shared_building_slots = 2
		add_building_construction = {
			type = arms_factory
			level = 2
			instant_build = yes
		}
	}
}
three_random_arms_factory = {
	random_owned_controlled_state = {
		limit = {
			free_building_slots = {
				building = arms_factory
				size > 0
				include_locked = yes
			}
			OR = {
				is_in_home_area = yes
				NOT = {
					owner = {
						any_owned_state = {
							free_building_slots = {
								building = arms_factory
								size > 0
								include_locked = yes
							}
							is_in_home_area = yes
						}
					}
				}
			}
		}
		add_extra_state_shared_building_slots = 2
		add_building_construction = {
			type = arms_factory
			level = 2
			instant_build = yes
		}
	}
	random_owned_controlled_state = {
		limit = {
			free_building_slots = {
				building = arms_factory
				size > 0
				include_locked = yes
			}
			OR = {
				is_in_home_area = yes
				NOT = {
					owner = {
						any_owned_state = {
							free_building_slots = {
								building = arms_factory
								size > 0
								include_locked = yes
							}
							is_in_home_area = yes
						}
					}
				}
			}
		}
		add_extra_state_shared_building_slots = 1
		add_building_construction = {
			type = arms_factory
			level = 1
			instant_build = yes
		}
	}
}
four_random_arms_factory = {
	random_owned_controlled_state = {
		limit = {
			free_building_slots = {
				building = arms_factory
				size > 0
				include_locked = yes
			}
			OR = {
				is_in_home_area = yes
				NOT = {
					owner = {
						any_owned_state = {
							free_building_slots = {
								building = arms_factory
								size > 0
								include_locked = yes
							}
							is_in_home_area = yes
						}
					}
				}
			}
		}
		add_extra_state_shared_building_slots = 2
		add_building_construction = {
			type = arms_factory
			level = 2
			instant_build = yes
		}
	}
	random_owned_controlled_state = {
		limit = {
			free_building_slots = {
				building = arms_factory
				size > 0
				include_locked = yes
			}
			OR = {
				is_in_home_area = yes
				NOT = {
					owner = {
						any_owned_state = {
							free_building_slots = {
								building = arms_factory
								size > 0
								include_locked = yes
							}
							is_in_home_area = yes
						}
					}
				}
			}
		}
		add_extra_state_shared_building_slots = 2
		add_building_construction = {
			type = arms_factory
			level = 2
			instant_build = yes
		}
	}
}
one_random_industrial_complex = {
	random_owned_controlled_state = {
		limit = {
			free_building_slots = {
				building = industrial_complex
				size > 0
				include_locked = yes
			}
			OR = {
				is_in_home_area = yes
				NOT = {
					owner = {
						any_owned_state = {
							free_building_slots = {
								building = industrial_complex
								size > 0
								include_locked = yes
							}
							is_in_home_area = yes
						}
					}
				}
			}
		}
		add_extra_state_shared_building_slots = 1
		add_building_construction = {
			type = industrial_complex
			level = 1
			instant_build = yes
		}
	}
}
two_random_industrial_complex = {
	random_owned_controlled_state = {
		limit = {
			free_building_slots = {
				building = industrial_complex
				size > 0
				include_locked = yes
			}
			OR = {
				is_in_home_area = yes
				NOT = {
					owner = {
						any_owned_state = {
							free_building_slots = {
								building = industrial_complex
								size > 0
								include_locked = yes
							}
							is_in_home_area = yes
						}
					}
				}
			}
		}
		add_extra_state_shared_building_slots = 2
		add_building_construction = {
			type = industrial_complex
			level = 2
			instant_build = yes
		}
	}
}
three_random_industrial_complex = {
	random_owned_controlled_state = {
		limit = {
			free_building_slots = {
				building = industrial_complex
				size > 0
				include_locked = yes
			}
			OR = {
				is_in_home_area = yes
				NOT = {
					owner = {
						any_owned_state = {
							free_building_slots = {
								building = industrial_complex
								size > 0
								include_locked = yes
							}
							is_in_home_area = yes
						}
					}
				}
			}
		}
		add_extra_state_shared_building_slots = 3
		add_building_construction = {
			type = industrial_complex
			level = 3
			instant_build = yes
		}
	}
}
four_random_industrial_complex = {
	random_owned_controlled_state = {
		limit = {
			free_building_slots = {
				building = industrial_complex
				size > 0
				include_locked = yes
			}
			OR = {
				is_in_home_area = yes
				NOT = {
					owner = {
						any_owned_state = {
							free_building_slots = {
								building = industrial_complex
								size > 0
								include_locked = yes
							}
							is_in_home_area = yes
						}
					}
				}
			}
		}
		add_extra_state_shared_building_slots = 2
		add_building_construction = {
			type = industrial_complex
			level = 2
			instant_build = yes
		}
	}
	random_owned_controlled_state = {
		limit = {
			free_building_slots = {
				building = industrial_complex
				size > 0
				include_locked = yes
			}
			OR = {
				is_in_home_area = yes
				NOT = {
					owner = {
						any_owned_state = {
							free_building_slots = {
								building = industrial_complex
								size > 0
								include_locked = yes
							}
							is_in_home_area = yes
						}
					}
				}
			}
		}
		add_extra_state_shared_building_slots = 2
		add_building_construction = {
			type = industrial_complex
			level = 2
			instant_build = yes
		}
	}
}
one_random_infrastructure = {
	random_owned_controlled_state = {
		limit = {
			free_building_slots = {
				building = infrastructure
				size > 0
				include_locked = yes
			}
			OR = {
				is_in_home_area = yes
				NOT = {
					owner = {
						any_owned_state = {
							free_building_slots = {
								building = infrastructure
								size > 0
								include_locked = yes
							}
							is_in_home_area = yes
						}
					}
				}
			}
		}
		add_building_construction = {
			type = infrastructure
			level = 2
			instant_build = yes
		}
	}
}
one_random_dockyard = {
	if = {
		limit = {
			NOT = {
				any_owned_state = {
					dockyard > 0
					free_building_slots = {
						building = dockyard
						size > 2
						include_locked = yes
					}
				}
			}
			any_owned_state = {
				is_coastal = yes
			}
		}
		random_owned_controlled_state = {
			limit = {
				is_coastal = yes
				free_building_slots = {
					building = dockyard
					size > 2
					include_locked = yes
				}
			}
			add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = dockyard
				level = 1
				instant_build = yes
			}
		}
	}
	if = {
		limit = {
			NOT = { has_country_flag = naval_effort_built }
			any_owned_state = {
				dockyard > 0
				free_building_slots = {
					building = dockyard
					size > 1
					include_locked = yes
				}
			}
		}
		random_owned_controlled_state = {
			limit = {
				dockyard > 0
				free_building_slots = {
					building = dockyard
					size > 1
					include_locked = yes
				}
			}
			add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = dockyard
				level = 1
				instant_build = yes
			}
		}
	}
	if = {
		limit = {
			NOT = { has_country_flag = naval_effort_built }
			NOT = {
				any_owned_state = {
					free_building_slots = {
						building = dockyard
						size > 1
						include_locked = yes
					}
				}
			}
		}
		random_state = {
			limit = {
				controller = { tag = ROOT }
				free_building_slots = {
					building = dockyard
					size > 1
					include_locked = yes
				}
			}
			add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = dockyard
				level = 1
				instant_build = yes
			}
		}
	}
}
two_random_dockyards = {
	if = {
		limit = {
			NOT = {
				any_owned_state = {
					dockyard > 0
					free_building_slots = {
						building = dockyard
						size > 2
						include_locked = yes
					}
				}
			}
			any_owned_state = {
				is_coastal = yes
			}
		}
		random_owned_controlled_state = {
			limit = {
				is_coastal = yes
				free_building_slots = {
					building = dockyard
					size > 2
					include_locked = yes
				}
			}
			add_extra_state_shared_building_slots = 2
			add_building_construction = {
				type = dockyard
				level = 2
				instant_build = yes
			}
		}
		set_country_flag = naval_effort_built
	}
	if = {
		limit = {
			NOT = { has_country_flag = naval_effort_built }
			any_owned_state = {
				dockyard > 0
				free_building_slots = {
					building = dockyard
					size > 2
					include_locked = yes
				}
			}
		}
		random_owned_controlled_state = {
			limit = {
				dockyard > 0
				free_building_slots = {
					building = dockyard
					size > 2
					include_locked = yes
				}
			}
			add_extra_state_shared_building_slots = 2
			add_building_construction = {
				type = dockyard
				level = 2
				instant_build = yes
			}
		}
		set_country_flag = naval_effort_built
	}
	if = {
		limit = {
			NOT = { has_country_flag = naval_effort_built }
			NOT = {
				any_owned_state = {
					free_building_slots = {
						building = dockyard
						size > 2
						include_locked = yes
					}
				}
			}
		}
		random_state = {
			limit = {
				controller = { tag = ROOT }
				free_building_slots = {
					building = dockyard
					size > 2
					include_locked = yes
				}
			}
			add_extra_state_shared_building_slots = 2
			add_building_construction = {
				type = dockyard
				level = 2
				instant_build = yes
			}
		}
	}
}
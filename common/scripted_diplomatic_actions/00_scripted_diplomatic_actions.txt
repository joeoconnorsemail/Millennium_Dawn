
scripted_diplomatic_actions = {
	##Yard1's Recall Volunteers mod integrated :D
	recall_volunteers = {
		allowed = {
			has_country_flag = allowed_recall_volunteers
			ROOT = { is_ai = no }
		}
		visible = { 
			has_country_flag = allowed_recall_volunteers
		}
		selectable = {
			# doing this for nicer tooltips
			if = {
				limit = {
					NOT = { has_volunteers_amount_from = { tag = ROOT count > 0 } }
				}
				custom_trigger_tooltip = {
					tooltip = RECALL_VOLUNTEERS_TOOLTIP
					always = no
				}
			}
			else = {
				custom_trigger_tooltip = {
					tooltip = RECALL_VOLUNTEERS_TOOLTIP_G
					always = yes
				}
			}
		}

		cost = 0
		command_power = 0

		requires_acceptance = no
		show_acceptance_on_action_button = no

		icon = 1

		send_description = RECALL_VOLUNTEERS_SEND_DESC

		complete_effect = {
			ROOT = { recall_volunteers_from = PREV }
		}
	}

	embargo_country = {
		allowed = {
			#ROOT = { is_ai = no }
			always = yes
		}
		visible = { 
			THIS = {
				NOT = { has_country_flag = embargo@ROOT }
			}
		}
		selectable = {
			custom_trigger_tooltip = {
				tooltip = EMBARGO_TOOLTIP
				THIS = {
					NOT = { has_country_flag = embargo@ROOT }
				}
			}
			THIS = { NOT = { is_in_faction_with = ROOT } }
			if = {
				limit = { 
					ROOT = {
						OR = {
							has_idea = NATO_member
							has_idea = Major_Non_NATO_Ally
						}
					} 
				}
				THIS = {
					NOT = {
						has_idea = NATO_member
						has_idea = Major_Non_NATO_Ally
					}
				}
			}

		}
		cost = 150
		command_power = 10
		can_be_sent = {
			always = no
		}
		requires_acceptance = no
		show_acceptance_on_action_button = no 
		icon = 1 
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: diplomatic action embargo"
			set_country_flag = embargo@ROOT
			add_opinion_modifier = { target = ROOT modifier = embargo }
			reverse_add_opinion_modifier = { target = ROOT modifier = embargo }
			add_opinion_modifier = { target = ROOT modifier = embargo_opinion }
			reverse_add_opinion_modifier = { target = ROOT modifier = embargo_opinion }
			if = {
				limit = {
					ROOT = { tag = USA }
					has_idea = USA_usaid
				}
				THIS = { remove_ideas = USA_usaid }
			}
		}

		send_description = EMBARGO_SEND_DESC

		ai_desire = {
			base = 0
			modifier = {
				ROOT = { 
					has_government = democratic
					has_opinion = {
						target = THIS
						value < 0
					}
				}
				THIS = { 
					any_enemy_country = { has_government = democratic }
				}
				add = 2
			}
			modifier = {
				ROOT = { 
					has_government = democratic
					has_opinion = {
						target = THIS
						value < -25
					}
				}
				THIS = { 
					any_enemy_country = { has_government = democratic }
				}
				add = 2
			}
			modifier = {
				ROOT = { 
					has_government = democratic
					has_opinion = {
						target = THIS
						value < -50
					}
				}
				THIS = { 
					any_enemy_country = { has_government = democratic }
				}
				add = 2
			}
			modifier = {
				ROOT = { 
					has_government = democratic
					has_opinion = {
						target = THIS
						value < -75
					}
				}
				THIS = { 
					any_enemy_country = { has_government = democratic }
				}
				add = 2
			}
			modifier = {
				ROOT = { 
					has_government = communism
					has_opinion = {
						target = THIS
						value < 0
					}
				}
				THIS = { 
					any_enemy_country = { has_government = communism }
				}
				add = 2
			}
			modifier = {
				ROOT = { 
					has_government = communism
					has_opinion = {
						target = THIS
						value < -25
					}
				}
				THIS = { 
					any_enemy_country = { has_government = communism }
				}
				add = 2
			}
			modifier = {
				ROOT = { 
					has_government = communism
					has_opinion = {
						target = THIS
						value < -50
					}
				}
				THIS = { 
					any_enemy_country = { has_government = communism }
				}
				add = 2
			}
			modifier = {
				ROOT = { 
					has_government = communism
					has_opinion = {
						target = THIS
						value < -75
					}
				}
				THIS = { 
					any_enemy_country = { has_government = communism }
				}
				add = 2
			}
			modifier = {
				ROOT = { 
					has_government = fascism
					has_opinion = {
						target = THIS
						value < 0
					}
				}
				THIS = { 
					any_enemy_country = { has_government = fascism }
				}
				add = 2
			}
			modifier = {
				ROOT = { 
					has_government = fascism
					has_opinion = {
						target = THIS
						value < -25
					}
				}
				THIS = { 
					any_enemy_country = { has_government = fascism }
				}
				add = 2
			}
			modifier = {
				ROOT = { 
					has_government = fascism
					has_opinion = {
						target = THIS
						value < -50
					}
				}
				THIS = { 
					any_enemy_country = { has_government = fascism }
				}
				add = 2
			}
			modifier = {
				ROOT = { 
					has_government = fascism
					has_opinion = {
						target = THIS
						value < -75
					}
				}
				THIS = { 
					any_enemy_country = { has_government = fascism }
				}
				add = 2
			}
			modifier = {
				ROOT = { 
					has_government = nationalist
					has_opinion = {
						target = THIS
						value < 0
					}
				}
				THIS = { 
					any_enemy_country = { has_government = nationalist }
				}
				add = 2
			}
			modifier = {
				ROOT = { 
					has_government = nationalist
					has_opinion = {
						target = THIS
						value < -25
					}
				}
				THIS = { 
					any_enemy_country = { has_government = nationalist }
				}
				add = 2
			}
			modifier = {
				ROOT = { 
					has_government = nationalist
					has_opinion = {
						target = THIS
						value < -50
					}
				}
				THIS = { 
					any_enemy_country = { has_government = nationalist }
				}
				add = 2
			}
			modifier = {
				ROOT = { 
					has_government = nationalist
					has_opinion = {
						target = THIS
						value < -75
					}
				}
				THIS = { 
					any_enemy_country = { has_government = nationalist }
				}
				add = 2
			}
			modifier = {
				ROOT = { 
					has_government = neutrality
					has_opinion = {
						target = THIS
						value < 0
					}
				}
				THIS = { 
					any_enemy_country = { has_government = neutrality }
				}
				add = 1
			}
			modifier = {
				ROOT = { 
					has_government = neutrality
					has_opinion = {
						target = THIS
						value < -25
					}
				}
				THIS = { 
					any_enemy_country = { has_government = neutrality }
				}
				add = 1
			}
			modifier = {
				ROOT = { 
					has_government = neutrality
					has_opinion = {
						target = THIS
						value < -50
					}
				}
				THIS = { 
					any_enemy_country = { has_government = neutrality }
				}
				add = 1
			}
			modifier = {
				ROOT = { 
					has_government = neutrality
					has_opinion = {
						target = THIS
						value < -75
					}
				}
				THIS = { 
					any_enemy_country = { has_government = neutrality }
				}
				add = 1
			}
			modifier = {
				threat > 25
				factor = 2
			}
			modifier = {
				threat > 50
				factor = 2
			}
			modifier = {
				threat > 75
				factor = 2
			}
			modifier = {
				THIS = { has_offensive_war = yes }
				factor = 2
			}
		}
	}

	lift_embargo_country = {
		allowed = {
			always = yes
		}
		visible = { 
			THIS = {
				has_country_flag = embargo@ROOT
			}
		}
		selectable = {
			custom_trigger_tooltip = {
				tooltip = LIFT_EMBARGO_TOOLTIP
				THIS = {
					has_country_flag = embargo@ROOT
				}
			}
		}
		cost = 50
		command_power = 0
		can_be_sent = {
			always = no
		}
		requires_acceptance = no
		show_acceptance_on_action_button = no
		icon = 1 
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: diplomatic action lift_embargo"
			remove_opinion_modifier = { target = ROOT modifier = embargo }
			remove_opinion_modifier = { target = ROOT modifier = embargo_opinion }
			THIS = {
				country_event = { id = diplo_action_tools.1 }
			}
			add_opinion_modifier = { target = ROOT modifier = recently_embargoed }
			reverse_add_opinion_modifier = { target = ROOT modifier = recently_embargoed }
			clr_country_flag = embargo@ROOT
		}

		send_description = LIFT_EMBARGO_SEND_DESC

		ai_desire = {
			factor = 0
			modifier = {
				THIS = { has_war = no }
				add = 1
			}
			modifier = {
				ROOT = {
					has_opinion = {
						target = THIS
						value > 0
					}
				}
				factor = 2
			}
			modifier = {
				ROOT = {
					has_opinion = {
						target = THIS
						value > 25
					}
				}
				factor = 2
			}
			modifier = {
				ROOT = {
					has_opinion = {
						target = THIS
						value > 50
					}
				}
				factor = 2
			}
			modifier = {
				ROOT = {
					has_opinion = {
						target = THIS
						value > 75
					}
				}
				factor = 2
			}
		}
	}
}

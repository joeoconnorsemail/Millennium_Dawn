ideas = {

	Vehicle_Company = {
	
		designer = yes
		
		EGY_kader_factory_vehicle_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea EGY_kader_factory_vehicle_company" }
		
			picture = Kader_Factory_EGY
			

			available = {
				OR = {
					original_tag = EGY
					controls_state = 215
				}
			}
			visible = {
				OR = {
					original_tag = EGY
					# controls_state = 215
					AND = {
						controls_state = 215
						has_better_than_AFV_3 = no
					}
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_AFV = 0.093
			}
			
			traits = {
				Cat_AFV_3
			
			}
			ai_will_do = {
				factor = 0.3 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 10 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_AFV_3 = yes
					factor = 0
				}
				modifier = {
					is_researching_afv = yes
					factor = 4000
				}
			}
			
		}
	}
	
	Infantry_Weapon_Company = {
	
		designer = yes
		
		EGY_kader_factory_infantry_weapon_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea EGY_kader_factory_infantry_weapon_company" }
		
			picture = Kader_Factory_EGY
			

			available = {
				OR = {
					original_tag = EGY
					controls_state = 215
				}
			}
			visible = {
				OR = {
					original_tag = EGY
					# controls_state = 215
					AND = {
						controls_state = 215
						has_better_than_INF_3 = no
					}
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_INF = 0.093
			}
			
			traits = {
				Cat_INF_3
			
			}
			ai_will_do = {
				factor = 0.3 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 5 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_INF_3 = yes
					factor = 0
				}
				modifier = {
					is_researching_infantry_equipment = yes
					factor = 1000
				}
				modifier = {
					AND = {
						is_researching_aa = yes
						has_AA_designer = yes
					}
					factor = 0
				}
				modifier = {
					AND = {
						is_researching_at = yes
						has_AT_designer = yes
					}
					factor = 0
				}
				modifier = {
					AND = {
						is_researching_land_drones = yes
						has_L_DRONE_designer = yes
					}
					factor = 0
				}
			}
			
		}
	}
	
	Vehicle_Company = {
	
		designer = yes
		
		EGY_helwan_machine_tools_vehicle_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea EGY_helwan_machine_tools_vehicle_company" }
		
			picture = Helwan_Machine_Tools_EGY
			

			available = {
				OR = {
					original_tag = EGY
					controls_state = 215
				}
			}
			visible = {
				OR = {
					original_tag = EGY
					# controls_state = 215
					AND = {
						controls_state = 215
						has_better_than_ARMOR_2 = no
					}
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_ARMOR = 0.062
			}
			
			traits = {
				Cat_ARMOR_2
			
			}
			ai_will_do = {
				factor = 0.2 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 10 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_ARMOR_2 = yes
					factor = 0
				}
				modifier = {
					is_researching_armor = yes
					factor = 1000
				}
				modifier = {
					AND = {
						is_researching_afv = yes
						has_AFV_designer = yes
					}
					factor = 0
				}
				modifier = {
					AND = {
						is_researching_tanks = yes
						has_TANKS_designer = yes
					}
					factor = 0
				}
				modifier = {
					AND = {
						is_researching_artillery = yes
						has_ARTILLERY_designer = yes
					}
					factor = 0
				}
			}
			
		}
	}
	
	Infantry_Weapon_Company = {
	
		designer = yes
		
		EGY_maadi_arms_infantry_weapon_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea EGY_maadi_arms_infantry_weapon_company" }
		
			picture = Maadi_Arms_EGY
			

			available = {
				OR = {
					original_tag = EGY
					controls_state = 215
				}
			}
			visible = {
				OR = {
					original_tag = EGY
					# controls_state = 215
					AND = {
						controls_state = 215
						has_better_than_INF_WEP_4 = no
					}
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_INF_WEP = 0.124
			}
			
			traits = {
				Cat_INF_WEP_4
			
			}
			ai_will_do = {
				factor = 0.4 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 5 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_INF_WEP_4 = yes
					factor = 0
				}
				modifier = {
					is_researching_infantry_weapons = yes
					factor = 4000
				}
			}
			
		}
	}
	
	Helicopter_Company = {
	
		designer = yes
		
		EGY_abhco_helicopter_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea EGY_abhco_helicopter_company" }
		
			picture = ABHCO_EGY
			

			available = {
				OR = {
					original_tag = EGY
					controls_state = 215
				}
			}
			visible = {
				OR = {
					original_tag = EGY
					# controls_state = 215
					AND = {
						controls_state = 215
						has_better_than_HELI_3 = no
					}
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_HELI = 0.093
			}
			
			traits = {
				Cat_HELI_3
			
			}
			ai_will_do = {
				factor = 0.3 #Most countries don't have decent airforces
				
				modifier = {
					has_tech = attack_helicopter2 #has semi-modern tech, most countries dont have it
					factor = 1
				}
				modifier = {
					has_tech = transport_helicopter2 #has semi-modern tech, most countries dont have it
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_HELI_3 = yes
					factor = 0
				}
				modifier = {
					is_researching_helicopters = yes
					factor = 4000
				}
			}
			
		}
	}
	
	Aircraft_Company = {
	
		designer = yes
		
		EGY_aoi_aircraft_aircraft_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea EGY_aoi_aircraft_aircraft_company" }
		
			picture = AOI_Aircraft_EGY
			

			available = {
				OR = {
					original_tag = EGY
					controls_state = 215
				}
			}
			visible = {
				OR = {
					original_tag = EGY
					# controls_state = 215
					AND = {
						controls_state = 215
						has_better_than_L_Fighter_3 = no
					}
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_L_Fighter = 0.093
			}
			
			traits = {
				Cat_L_Fighter_3
			
			}
			ai_will_do = {
				factor = 0.3 #Most countries don't have decent airforces
				
				modifier = {
					or = {
						has_tech = AS_Fighter2 #has semi-modern tech
						has_tech = MR_Fighter2
						has_tech = Strike_fighter2
						has_tech = L_Strike_fighter2
						has_tech = Air_UAV1
					}
					factor = 1
				}
				modifier = {
					or = {
						has_tech = strategic_bomber3 #has semi-modern tech, most countries dont have it
						has_tech = transport_plane2
						has_tech = naval_plane3
						has_tech = cas2
					}
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_L_Fighter_3 = yes
					factor = 0
				}
				modifier = {
					is_researching_light_fighter = yes
					factor = 4000
				}
			}
			
		}
	}
	
	Ship_Company = {
	
		designer = yes
		
		EGY_alexandria_shipyard_ship_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea EGY_alexandria_shipyard_ship_company" }
		
			picture = Alexandria_Shipyard_EGY
			

			available = {
				OR = {
					original_tag = EGY
					controls_state = 216
				}
			}
			visible = {
				OR = {
					original_tag = EGY
					# controls_state = 216
					AND = {
						controls_state = 216
						has_better_than_NAVAL_EQP_2 = no
					}
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_NAVAL_EQP = 0.062
			}
			
			traits = {
				Cat_NAVAL_EQP_2
			
			}
			ai_will_do = {
				factor = 0.2
				
				modifier = {
					has_navy_size = { size > 25 } #has a large navy
					factor = 1
				}
				modifier = {
					num_of_naval_factories > 3 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					NOT = { #need to have ports
						any_owned_state = {
							is_coastal = yes
						}
					}
					factor = 10
				}
				modifier = {
					has_better_than_NAVAL_EQP_2 = yes
					factor = 0
				}
				modifier = {
					is_researching_naval_equipment = yes
					factor = 1000
				}
				modifier = {
					AND = {
						is_researching_carrier = yes
						has_CARRIER_designer = yes
					}
					factor = 0
				}
				modifier = {
					AND = {
						is_researching_surface_ship = yes
						has_SURFACE_SHIP_designer = yes
					}
					factor = 0
				}
			}
			
		}
	}
	
	Vehicle_Company = {
	
		designer = yes
		
		EGY_abu_zaabal_tank_factory_vehicle_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea EGY_abu_zaabal_tank_factory_vehicle_company" }
		
			picture = Abu_Zaabal_Tank_Factory_EGY
			

			available = {
				OR = {
					original_tag = EGY
					controls_state = 215
				}
			}
			visible = {
				OR = {
					original_tag = EGY
					# controls_state = 215
					AND = {
						controls_state = 215
						has_better_than_ARMOR_5 = no
					}
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_ARMOR = 0.155
			}
			
			traits = {
				Cat_ARMOR_5
			
			}
			ai_will_do = {
				factor = 0.5 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 10 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_ARMOR_5 = yes
					factor = 0
				}
				modifier = {
					is_researching_armor = yes
					factor = 1000
				}
				modifier = {
					AND = {
						is_researching_afv = yes
						has_AFV_designer = yes
					}
					factor = 0
				}
				modifier = {
					AND = {
						is_researching_tanks = yes
						has_TANKS_designer = yes
					}
					factor = 0
				}
				modifier = {
					AND = {
						is_researching_artillery = yes
						has_ARTILLERY_designer = yes
					}
					factor = 0
				}
			}
			
		}
	}
	
}

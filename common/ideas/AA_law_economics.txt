ideas = {
	economic_cycles = {
		law = yes
		use_list_view = yes
		depression = {
			cost = 300
			removal_cost = -1
			level = 6
			available = {
				has_idea = depression
			}
			on_add = {
				if = {
					limit = { NOT = { has_country_flag = disable_cycle_costs } }
					custom_effect_tooltip = depression_TT
					custom_effect_tooltip = econ_cycle_upg_cost_TT
					custom_effect_tooltip = economic_cycle_TT
					hidden_effect = {
						set_variable = { law_attitude = 1 }
						set_variable = { law_change = 6 }
						subtract_from_variable = { law_change = economic_cycles }
						law_attitude_change = yes
						set_variable = { economic_cycles = 6 }
						calculate_tax_gain = yes
					}
				}
				clear_previous_economic_drift = yes
				update_drift_depression = yes
				log = "[GetDateText]: [Root.GetName]: add idea depression"
			}
			modifier = {
				stability_factor = -0.1
			}
			ai_will_do = {
				factor = 0
			}
			cancel_if_invalid = no
		}
		recession = {
			cost = 150
			removal_cost = -1
			level = 5
			available = {
				OR = {
					has_idea = depression
					has_idea = recession
				}
			}
			on_add = {
				if = {
					limit = { NOT = { has_country_flag = disable_cycle_costs } }
					custom_effect_tooltip = recession_TT
					custom_effect_tooltip = econ_cycle_upg_cost_TT
					custom_effect_tooltip = economic_cycle_TT
					hidden_effect = {
						set_variable = { law_attitude = 1 }
						set_variable = { law_change = 5 }
						subtract_from_variable = { law_change = economic_cycles }
						law_attitude_change = yes
						set_variable = { economic_cycles = 5 }
						calculate_tax_gain = yes
						subtract_from_variable = { var = treasury value = econ_cycle_upg_cost }
					}
				}
				clear_previous_economic_drift = yes
				update_drift_recession = yes
				log = "[GetDateText]: [Root.GetName]: add idea recession"
			}
			modifier = {
				production_speed_buildings_factor = 0.30
				stability_factor = -0.05
			}
			ai_will_do = {
				factor = 1
				modifier = { # Treat improving economy as extremely urgent if in depression
					factor = 320
					has_idea = depression
				}
				modifier = { # Extremely corrupt nations less likely to invest in country's future
					factor = 0.2
					has_idea = depression
					OR = {
						has_idea = paralyzing_corruption
						has_idea = crippling_corruption
					}
				}
				modifier = { # Corrupt nations somewhat less likely to invest in country's future
					factor = 0.5
					has_idea = depression
					OR = {
						has_idea = rampant_corruption
						has_idea = unrestrained_corruption
						has_idea = widespread_corruption
					}
				}
				modifier = { # Set ai_will_do to 0 if in recession to encourage AI to get out of recession - experimental
					factor = 0
					has_idea = recession
				}
				modifier = { # At war, don't focus efforts on economics
					factor = 0.5
					has_idea = depression
					has_war = yes
				}
			}
			cancel_if_invalid = no
		}
		stagnation = {
			cost = 200
			removal_cost = -1
			level = 4
			available = {
				OR = {
					has_idea = recession
					has_idea = stagnation
				}
			}
			on_add = {
				if = {
					limit = { NOT = { has_country_flag = disable_cycle_costs } }
					custom_effect_tooltip = stagnation_TT
					custom_effect_tooltip = econ_cycle_upg_cost_TT
					custom_effect_tooltip = economic_cycle_TT
					hidden_effect = {
						set_variable = { law_attitude = 1 }
						set_variable = { law_change = 4 }
						subtract_from_variable = { law_change = economic_cycles }
						law_attitude_change = yes
						set_variable = { economic_cycles = 4 }
						calculate_tax_gain = yes
						subtract_from_variable = { var = treasury value = econ_cycle_upg_cost }
					}
				}
				clear_previous_economic_drift = yes
				update_drift_stagnation = yes
				log = "[GetDateText]: [Root.GetName]: add idea stagnation"
			}
			modifier = {
				production_speed_buildings_factor = 0.75
				stability_factor = -0.02
			}
			ai_will_do = {
				factor = 1
				modifier = { # Treat improving economy as very urgent if in recession
					factor = 80
					has_idea = recession
				}
				modifier = { # Extremely corrupt nations less likely to invest in country's future
					factor = 0.2
					has_idea = recession
					OR = {
						has_idea = paralyzing_corruption
						has_idea = crippling_corruption
					}
				}
				modifier = { # Corrupt nations somewhat less likely to invest in country's future
					factor = 0.5
					has_idea = recession
					OR = {
						has_idea = rampant_corruption
						has_idea = unrestrained_corruption
						has_idea = widespread_corruption
					}
				}
				modifier = { # Low GDP nations more likely to want higher economic growth rates
					factor = 12
					has_idea = recession
					OR = {
						has_idea = gdp_1
						has_idea = gdp_2
					}
				}
				modifier = {
					factor = 6
					has_idea = recession
					OR = {
						has_idea = gdp_3
						has_idea = gdp_4
						has_idea = gdp_5
					}
				}
				modifier = { # Set ai_will_do to 0.1 if in stagnation to encourage AI to get out of stagnation - experimental
					factor = 0.1
					has_idea = stagnation
				}
				modifier = { # At war, don't focus efforts on economics
					factor = 0.5
					has_idea = recession
					has_war = yes
				}
			}
			cancel_if_invalid = no
		}
		stable_growth = {
			cost = 250
			removal_cost = -1
			level = 3
			available = {
				OR = {
					has_idea = stagnation
					has_idea = stable_growth
				}
			}
			on_add = {
				if = {
					limit = { NOT = { has_country_flag = disable_cycle_costs } }
					custom_effect_tooltip = econ_cycle_upg_cost_TT
					custom_effect_tooltip = economic_cycle_TT
					hidden_effect = {
						set_variable = { law_attitude = 1 }
						set_variable = { law_change = 3 }
						subtract_from_variable = { law_change = economic_cycles }
						law_attitude_change = yes
						set_variable = { economic_cycles = 3 }
						calculate_tax_gain = yes
						subtract_from_variable = { var = treasury value = econ_cycle_upg_cost }
					}
				}
				clear_previous_economic_drift = yes
				log = "[GetDateText]: [Root.GetName]: add idea stable_growth"
			}
			default = yes
			modifier = { production_speed_buildings_factor = 0.9 }
			ai_will_do = {
				factor = 2 # Stable growth should be goal for AI most of the time
				modifier = {
					factor = 12 # Nations should consider reviving a stagnant economy important.
					has_idea = stagnation
				}
				modifier = { # Extremely corrupt nations less likely to invest in country's future
					factor = 0.2
					has_idea = stagnation
					OR = {
						has_idea = paralyzing_corruption
						has_idea = crippling_corruption
					}
				}
				modifier = { # Corrupt nations somewhat less likely to invest in country's future
					factor = 0.5
					has_idea = stagnation
					OR = {
						has_idea = rampant_corruption
						has_idea = unrestrained_corruption
						has_idea = widespread_corruption
					}
				}
				modifier = { # Low GDP nations more likely to want higher economic growth rates
					factor = 10
					has_idea = stagnation
					OR = {
						has_idea = gdp_1
						has_idea = gdp_2
					}
				}
				modifier = {
					factor = 5
					has_idea = stagnation
					OR = {
						has_idea = gdp_3
						has_idea = gdp_4
						has_idea = gdp_5
					}
				}
				modifier = {
					factor = 10
					has_idea = stagnation
					has_political_power > 750 # If has a great abundance of PP, emphasize growth further I guess? Experimental.
				}
				modifier = {
					factor = 0.5 # Should encourage growth when possible
					has_idea = stable_growth
				}
				modifier = {
					factor = 0.25 # Country shouldn't focus too much on economic growth when at war, but should still want to get to stable growth at a lower priority
					has_idea = stagnation
					has_war = yes
				}
			}
			cancel_if_invalid = no
		}
		fast_growth = {
			cost = 300
			removal_cost = -1
			level = 2
			available = {
				OR = {
					has_idea = stable_growth
					has_idea = fast_growth
				}
			}
			on_add = {
				if = {
					limit = { NOT = { has_country_flag = disable_cycle_costs } }
					custom_effect_tooltip = fast_growth_TT
					custom_effect_tooltip = econ_cycle_upg_cost_TT
					custom_effect_tooltip = economic_cycle_TT
					hidden_effect = {
						set_variable = { law_attitude = 1 }
						set_variable = { law_change = 2 }
						subtract_from_variable = { law_change = economic_cycles }
						law_attitude_change = yes
						set_variable = { economic_cycles = 2 }
						calculate_tax_gain = yes
						subtract_from_variable = { var = treasury value = econ_cycle_upg_cost }
					}
				}
				clear_previous_economic_drift = yes
				update_drift_fast_growth = yes
				log = "[GetDateText]: [Root.GetName]: add idea fast_growth"
			}
			modifier = {
				production_speed_buildings_factor = 1.03
				stability_factor = 0.02
			}
			ai_will_do = {
				factor = 1
				modifier = {
					factor = 12 # If an abundance of PP is accumulated, grow economy.
					has_idea = stable_growth
					has_political_power > 500
				}
				modifier = { # Extremely corrupt nations less likely to invest in country's future
					factor = 0.2
					has_idea = stable_growth
					OR = {
						has_idea = paralyzing_corruption
						has_idea = crippling_corruption
					}
				}
				modifier = { # Corrupt nations somewhat less likely to invest in country's future
					factor = 0.5
					has_idea = stable_growth
					OR = {
						has_idea = rampant_corruption
						has_idea = unrestrained_corruption
						has_idea = widespread_corruption
					}
				}
				modifier = { # Low GDP nations more likely to want higher economic growth rates
					factor = 12
					has_idea = stable_growth
					OR = {
						has_idea = gdp_1
						has_idea = gdp_2
					}
				}
				modifier = {
					factor = 6
					has_idea = stable_growth
					OR = {
						has_idea = gdp_3
						has_idea = gdp_4
						has_idea = gdp_5
					}
				}
				modifier = {
					factor = 0.5 # Should encourage growth at low priority when possible
					has_idea = fast_growth
				}
				modifier = { # Don't bother with economic growth if at war
					factor = 0.1
					has_war = yes
					has_idea = stable_growth
				}
			}
			cancel_if_invalid = no
		}
		economic_boom = {
			cost = 350
			removal_cost = -1
			level = 1
			available = {
				OR = {
					has_idea = fast_growth
					has_idea = economic_boom
				}
			}
			on_add = {
				if = {
					limit = { NOT = { has_country_flag = disable_cycle_costs } }
					custom_effect_tooltip = economic_boom_TT
					custom_effect_tooltip = econ_cycle_upg_cost_TT
					custom_effect_tooltip = economic_cycle_TT
					hidden_effect = {
						set_variable = { law_attitude = 1 }
						set_variable = { law_change = 1 }
						subtract_from_variable = { law_change = economic_cycles }
						law_attitude_change = yes
						set_variable = { economic_cycles = 1 }
						calculate_tax_gain = yes
						subtract_from_variable = { var = treasury value = econ_cycle_upg_cost }
					}
				}
				clear_previous_economic_drift = yes
				update_drift_economic_boom = yes
				log = "[GetDateText]: [Root.GetName]: add idea economic_boom"
			}
			modifier = {
				production_speed_buildings_factor = 1.13
				stability_factor = 0.04
			}
			ai_will_do = {
				factor = 1
				modifier = { # Low GDP nations more likely to want higher economic growth rates
					factor = 6
					has_idea = stable_growth
					OR = {
						has_idea = gdp_1
						has_idea = gdp_2
					}
				}
				modifier = {
					factor = 3
					has_idea = stable_growth
					OR = {
						has_idea = gdp_3
						has_idea = gdp_4
						has_idea = gdp_5
					}
				}
				modifier = {
					factor = 4
					has_political_power > 990
				}
				modifier = { # Don't bother with economic growth if at war
					factor = 0.1
					has_idea = fast_growth
					has_war = yes
				}
			}
			cancel_if_invalid = no
		}
	}
	trade_laws = {
		law = yes
		use_list_view = yes
		md_closed_economy = { #No Exports
			cost = 150
			removal_cost = -1
			level = 1
			available = {
				has_elections = no
				OR = {
					has_government = nationalist
					has_government = fascism
				}
			}
			on_add = {
				calculate_resource_sale_rate = yes
				log = "[GetDateText]: [Root.GetName]: add idea closed_economy"
			}
			modifier = {
				min_export = 0.10
				trade_opinion_factor = -0.50
				consumer_goods_factor = -0.06
			}

			ai_will_do = {
				factor = 0
				modifier = {
					has_elections = no
					add = 3
				}
			}
			cancel_if_invalid = no
		}
		consumption_economy = { #Despite consuming the majority of your resources. You still export around 20%
			cost = 150
			removal_cost = -1
			level = 2
			modifier = {
				min_export = 0.20
				trade_opinion_factor = 0.2
				consumer_goods_factor = -0.03
			}
			on_add = {
				calculate_resource_sale_rate = yes
				log = "[GetDateText]: [Root.GetName]: add idea consumption_economy"
			}
			ai_will_do = {
				factor = 0
			}
			cancel_if_invalid = no
		}
		semi_consumption_economy = {
			cost = 150
			removal_cost = -1
			level = 3
			modifier = {
				min_export = 0.40
				trade_opinion_factor = 0.10
				consumer_goods_factor = -0.02
			}
			on_add = {
				calculate_resource_sale_rate = yes
				log = "[GetDateText]: [Root.GetName]: add idea semi_consumption_economy"
			}
			ai_will_do = {
				factor = 0
			}
			cancel_if_invalid = no
		}
		mixed_economy = {
			cost = 150
			removal_cost = -1
			level = 4
			available = {
			}
			modifier = {
				min_export = 0.5
			}
			on_add = {
				calculate_resource_sale_rate = yes
				log = "[GetDateText]: [Root.GetName]: add idea mixed_economy"
			}
			default = yes
			ai_will_do = {
				factor = 0
			}
			cancel_if_invalid = no
		}
		export_economy = {
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea export_economy"
				calculate_resource_sale_rate = yes
			}
			cost = 150
			removal_cost = -1
			level = 5
			modifier = {
				min_export = 0.65
				consumer_goods_factor = 0.03
			}
			ai_will_do = {
				factor = 0
			}
			cancel_if_invalid = no
		}
		globalized_trade_economy = { #Major economic powerhouse. We export all GOODs equally.
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea globalized_trade_economy"
				calculate_resource_sale_rate = yes
			}
			cost = 150
			removal_cost = -1
			level = 6
			modifier = {
				min_export = 0.8
				consumer_goods_factor = 0.06
			}
			ai_will_do = {
				factor = 0
			}
			cancel_if_invalid = no
		}
	}
}

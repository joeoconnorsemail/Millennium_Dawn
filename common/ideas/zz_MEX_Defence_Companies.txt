ideas = {

	Infantry_Weapon_Company = {
	
		designer = yes
		
		MEX_dgime_infantry_weapon_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea MEX_dgime_infantry_weapon_company" }
		
			picture = DGIME_MEX
			

			available = {
				OR = {
					tag = MEX
					controls_state = 835
				}
			}
			visible = {
				OR = {
					tag = MEX
					controls_state = 835
				}
			}
			cost = 150
			
			removal_cost = 10
			
			research_bonus = {
				Cat_INF_WEP = 0.124
			}
			
			traits = {
				Cat_INF_WEP_4
			
			}
			ai_will_do = {
				factor = 0.4 #All countries need a land army, vehicles are part of modern warfare
				
				modifier = {
					num_of_military_factories > 5 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_INF_WEP_4 = yes
					factor = 0
				}
				modifier = {
					is_researching_infantry_weapons = yes
					factor = 4000
				}
			}
			
		}
	}
	
}
